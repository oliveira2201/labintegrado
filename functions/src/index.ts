// Import all needed modules.
import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import * as algoliasearch from "algoliasearch";

// Set up Firestore.
admin.initializeApp();
const db = admin.firestore();

// Atualizar custom claims
export const mirrorCustomClaims = functions.firestore
  .document("usuarios-claims/{uid}")
  .onWrite(async (change, context) => {
    const auth = admin.auth();
    const newClaims = change.after.data() || {};

    const uid = context.params.uid;
    console.log(`Token adicionada para ${uid}`, newClaims);
    await auth.setCustomUserClaims(uid, newClaims);
  });
