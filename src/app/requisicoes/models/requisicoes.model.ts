import { firestore } from 'firebase';
import Timestamp = firestore.Timestamp;

export interface Requisicao {
  id: string;
  nomeAnimal: string;
  especie: string;
  raca: string;
  sexo: string;
  idade: string;
  tutor: string;
  veterinarioSolicitante: string;
  clinicaSolicitante: string;
  coleta: Date;
  resultado: Date;
  observacoes: string;
  exames: ExameRequisicao[];
}

export interface ExameRequisicao {
  id: string;
  nome: string;
  categoria: string;
  valor: string;
  especie: string;
  idadeMinima: string;
  idadeMaxima: string;
  situacao: string;
  campos: CampoRequisicao[];
}

export interface CampoRequisicao {
  nome: string;
  unidade: string;
  tipoDeDado: string;
  subcategoria: string;
  idadeMinima: string;
  idadeMaxima: string;
  valorMinimo: string;
  valorMaximo: string;
  resultado: string;
}
