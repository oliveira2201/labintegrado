import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'criar',
        loadChildren: () =>
          import(
            './pages/requisicoes-modificar/requisicoes-modificar.module'
          ).then(m => m.RequisicoesModificarPageModule)
      },
      {
        path: 'editar/:id',
        loadChildren: () =>
          import(
            './pages/requisicoes-modificar/requisicoes-modificar.module'
          ).then(m => m.RequisicoesModificarPageModule)
      },
      {
        path: '',
        loadChildren: () =>
          import('./pages/requisicoes-lista/requisicoes-lista.module').then(
            m => m.RequisicoesListaPageModule
          )
      }
    ]
  },
  {
    path: 'requisicoes-lista',
    loadChildren: () =>
      import('./pages/requisicoes-lista/requisicoes-lista.module').then(
        m => m.RequisicoesListaPageModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequisicoesRoutingModule {}
