import { Injectable } from "@angular/core";
import { Repository } from "src/app/core/classes/repository.class";
import { AngularFirestore } from "@angular/fire/firestore";
import { Requisicao } from "../../models/requisicoes.model";
import { AuthService } from "src/app/core/services/auth.service";
import { Observable, BehaviorSubject, combineLatest, of } from "rxjs";
import { switchMap, map, filter } from "rxjs/operators";

@Injectable({
  providedIn: "root",
})
export class RequisicoesService extends Repository<Requisicao> {
  items$: Observable<Requisicao[]>;
  estadoFiltro$: BehaviorSubject<string | null>;
  exameFiltro$: BehaviorSubject<string | null>;
  usuarioFiltro$: BehaviorSubject<string | null>;
  nomeAnimalFiltro$: BehaviorSubject<string | null>;

  constructor(db: AngularFirestore, private auth: AuthService) {
    super(db);
    this.estadoFiltro$ = new BehaviorSubject(null);
    this.exameFiltro$ = new BehaviorSubject(null);
    this.usuarioFiltro$ = new BehaviorSubject(null);
    this.nomeAnimalFiltro$ = new BehaviorSubject(null);
    // @ts-ignore
    this.items$ = combineLatest([
      this.estadoFiltro$,
      this.exameFiltro$,
      this.usuarioFiltro$,
      this.nomeAnimalFiltro$,
    ]).pipe(
      switchMap(([situacao, exame, usuarioId, nomeAnimal]) => {
        return combineLatest(
          db
            .collection("requisicoes", (ref) => {
              let query:
                | firebase.firestore.CollectionReference
                | firebase.firestore.Query = ref;
              if (situacao) {
                query = query.where("situacao", "==", situacao);
              }
              if (exame) {
                query = query.where("exame", "==", exame).orderBy("nomeAnimal");
              }
              if (usuarioId) {
                query = query.where("usuarioAssociado.uid", "==", usuarioId);
              }
              return query;
            })
            .valueChanges(),
          of(nomeAnimal)
        );
      }),
      map(([item, nomeAnimal]) => {
        if (item && nomeAnimal) {
          const filtrado = item.filter(
            (reg) =>
              //@ts-ignore
              reg.nomeAnimal.toLowerCase().includes(nomeAnimal.toLowerCase()) ||
              //@ts-ignore
              reg.tutor.toLowerCase().includes(nomeAnimal.toLowerCase()) ||
              //@ts-ignore
              reg.veterinarioSolicitante
                .toLowerCase()
                .includes(nomeAnimal.toLowerCase())
          );
          return filtrado;
        } else {
          return item;
        }
      }),
      // map((item) => item.sort((a, b) => b.coleta.seconds - a.coleta.seconds)),
      map((item) =>
        //@ts-ignore
        item.sort((a, b) => b.resultado.seconds - a.resultado.seconds)
      )
    );
    this.init();
  }

  filtrarPorSituacao(situacao: string | null) {
    this.estadoFiltro$.next(situacao);
  }

  filtrarPorExame(exame: string | null) {
    this.exameFiltro$.next(exame);
  }

  filtrarPorUsuario(usuarioId: string | null) {
    this.usuarioFiltro$.next(usuarioId);
  }

  filtrarPorNomeAnimal(nomeAnimal: string | null) {
    this.nomeAnimalFiltro$.next(nomeAnimal);
  }

  private async init() {
    this.setCollection("/requisicoes", (ref) => ref);
    this.auth.authState$.subscribe((res) => {
      res.getIdTokenResult().then((token) => {
        if (token.claims.admin) {
          this.filtrarPorUsuario(null);
        } else {
          this.filtrarPorUsuario(res.uid);
        }
      });
    });
  }
}
