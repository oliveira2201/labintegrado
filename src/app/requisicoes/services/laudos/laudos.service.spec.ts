import { TestBed } from '@angular/core/testing';

import { LaudosService } from './laudos.service';

describe('RelatoriosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LaudosService = TestBed.get(LaudosService);
    expect(service).toBeTruthy();
  });
});
