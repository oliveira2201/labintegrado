import { NgModule } from "@angular/core";
import { RequisicoesListaPage } from "./requisicoes-lista.page";
import { Routes, RouterModule } from "@angular/router";
import { RequisicoesListaItemComponent } from "./lista-card/requisicoes-lista-item.component";
import { SharedModule } from "src/app/shared/shared.module";
import { VirtualScrollerModule } from "ngx-virtual-scroller";

const routes: Routes = [
  {
    path: "",
    component: RequisicoesListaPage,
  },
];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes), VirtualScrollerModule],
  declarations: [RequisicoesListaPage, RequisicoesListaItemComponent],
})
export class RequisicoesListaPageModule {}
