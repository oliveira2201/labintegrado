import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RequisicoesListaPage } from './requisicoes-lista.page';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { RequisicoesListaItemComponent } from './lista-card/requisicoes-lista-item.component';
import { RequisicoesService } from '../../services/requisicoes/requisicoes.service';
import { Observable } from 'rxjs';

describe('RequisicoesListaPage', () => {
  let component: RequisicoesListaPage;
  let fixture: ComponentFixture<RequisicoesListaPage>;
  const requisicoesServiceStub = {
    obterPorId: () => {
      return new Observable(subscriber => {
        subscriber.next({
          nomeAnimal: 'animalNome'
        });
      });
    }
  };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RequisicoesListaPage, RequisicoesListaItemComponent],
      imports: [IonicModule.forRoot(), SharedModule, RouterModule.forRoot([])],
      providers: [{ provide: RequisicoesService, useValue: requisicoesServiceStub }]
    }).compileComponents();

    fixture = TestBed.createComponent(RequisicoesListaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('deve ser criado', () => {
    expect(component).toBeTruthy();
  });
});
