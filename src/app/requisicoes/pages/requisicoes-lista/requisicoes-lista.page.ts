import { Component, OnInit } from "@angular/core";
import { RequisicoesService } from "../../services/requisicoes/requisicoes.service";
import { Observable } from "rxjs";
import { Requisicao } from "../../models/requisicoes.model";
import { NavController } from "@ionic/angular";

@Component({
  selector: "app-requisicoes-lista",
  templateUrl: "./requisicoes-lista.page.html",
  styleUrls: ["./requisicoes-lista.page.scss"],
})
export class RequisicoesListaPage {
  requisicoes$: Observable<Requisicao[]>;
  situacao: string;
  constructor(
    public requisicoesService: RequisicoesService,
    private navCtrl: NavController
  ) {}

  atualizarRequisicao(requisicao: Requisicao): void {
    this.navCtrl.navigateForward(`/requisicoes/editar/${requisicao.id}`);
  }

  ionViewDidEnter() {
    this.requisicoes$ = this.requisicoesService.obterTodos();
  }
  filtrar(ev) {
    this.requisicoesService.filtrarPorSituacao(ev.target.value);
  }
  filtrarTexto(ev) {
    this.requisicoesService.filtrarPorNomeAnimal(ev.target.value);
  }
  onDelete(ev) {}
}
