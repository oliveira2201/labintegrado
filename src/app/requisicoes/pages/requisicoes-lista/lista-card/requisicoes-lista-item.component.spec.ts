import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RequisicoesListaItemComponent } from './requisicoes-lista-item.component';
import * as firebase from 'firebase';

describe('RequisicoesListaItemComponent', () => {
  let component: RequisicoesListaItemComponent;
  let fixture: ComponentFixture<RequisicoesListaItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RequisicoesListaItemComponent],
      imports: [IonicModule.forRoot()]
    }).compileComponents();
    fixture = TestBed.createComponent(RequisicoesListaItemComponent);
    component = fixture.componentInstance;
    component.requisicao = {
      nomeAnimal: 'Animal',
      coleta: firebase.firestore.Timestamp.fromDate(new Date('2020-01-12T00:05Z')),
      resultado: firebase.firestore.Timestamp.fromDate(new Date('2020-01-12T00:06Z')),
      tutor: 'TutorNome',
      veterinarioSolicitante: 'VeterinarioNome',
      situacao: 'Situacao',
      exames: [
        {
          nome: 'Exame1'
        },
        {
          nome: 'Exame2'
        }
      ]
    };
    fixture.detectChanges();
  }));

  it('deve ser criado', () => {
    expect(component).toBeTruthy();
  });
  describe('deve ter os campos:', () => {
    it('Nome do animal', () => {
      const texto = Array.from(document.querySelectorAll('ion-card-title')).find(
        el => el.textContent === 'Animal'
      );
      expect(texto).toBeTruthy();
    });
    it('Situação', () => {
      const texto = Array.from(document.querySelectorAll('ion-col')).find(
        el => el.textContent === 'Situacao'
      );
      expect(texto).toBeTruthy();
    });
    it('Data de coleta no formato dd/mm/aaaa hh:mm', () => {
      const texto = Array.from(document.querySelectorAll('small')).find(
        el => el.textContent === ' 11/01/2020 21:05 '
      );
      expect(texto).toBeTruthy();
    });
    it('Data do resultado no formato dd/mm/aaaa hh:mm', () => {
      const texto = Array.from(document.querySelectorAll('small')).find(
        el => el.textContent === ' 11/01/2020 21:06 '
      );
      expect(texto).toBeTruthy();
    });
    it('Tutor', () => {
      const texto = Array.from(document.querySelectorAll('ion-label')).find(
        el => el.textContent === 'Tutor'
      );
      expect(texto).toBeTruthy();
    });
    it('Veterinário', () => {
      const texto = Array.from(document.querySelectorAll('ion-label')).find(
        el => el.textContent === 'Veterinário'
      );
      expect(texto).toBeTruthy();
    });
    it('Lista de exames', () => {
      const texto1 = Array.from(document.querySelectorAll('ion-badge')).find(
        el => el.textContent === ' Exame1 '
      );
      expect(texto1).toBeTruthy();
      const texto2 = Array.from(document.querySelectorAll('ion-badge')).find(
        el => el.textContent === ' Exame2 '
      );
      expect(texto2).toBeTruthy();
    });
  });
  describe('o campo situação deve mudar de cor, dependendo da situação', () => {
    it('Aguardando Amostras', () => {
      component.requisicao.situacao = 'Aguardando Amostras';
      fixture.detectChanges();
      const classe = document.querySelector('.req-aguardandoAmostras');
      expect(classe).toBeTruthy();
    });
    it('Aguardando Liberação', () => {
      component.requisicao.situacao = 'Aguardando Liberação';
      fixture.detectChanges();
      const classe = document.querySelector('.req-aguardandoLiberacao');
      expect(classe).toBeTruthy();
    });
    it('Aguardando Resultados', () => {
      component.requisicao.situacao = 'Aguardando Resultados';
      fixture.detectChanges();
      const classe = document.querySelector('.req-aguardandoResultados');
      expect(classe).toBeTruthy();
    });
    it('Finalizado', () => {
      component.requisicao.situacao = 'Finalizado';
      fixture.detectChanges();
      const classe = document.querySelector('.req-finalizado');
      expect(classe).toBeTruthy();
    });
  });
  describe('os exames devem mudar de cor, dependendo da situação', () => {
    it('Aguardando Liberação', () => {
      component.requisicao.exames[0].situacao = 'Aguardando Liberação';
      fixture.detectChanges();
      const classe = document.querySelector('.exame-aguardandoLiberacao');
      expect(classe).toBeTruthy();
    });
    it('Aguardando Resultados', () => {
      component.requisicao.exames[0].situacao = 'Aguardando Resultados';
      fixture.detectChanges();
      const classe = document.querySelector('.exame-aguardandoResultados');
      expect(classe).toBeTruthy();
    });
    it('Finalizado', () => {
      component.requisicao.exames[0].situacao = 'Finalizado';
      fixture.detectChanges();
      const classe = document.querySelector('.exame-Finalizado');
      expect(classe).toBeTruthy();
    });
  });
  it('deve emitir o evento "atualizar" com a requisição ao ser clicado', () => {
    const card = document.querySelector('ion-card');
    spyOn(component.atualizar, 'emit');
    card.click();
    expect(component.atualizar.emit).toHaveBeenCalledWith(component.requisicao);
  });
});
