import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

/**
 * Card que mostra os principais dados de cada requisição.
 */
@Component({
  selector: 'app-requisicoes-lista-item',
  templateUrl: './requisicoes-lista-item.component.html',
  styleUrls: ['./requisicoes-lista-item.component.scss']
})
export class RequisicoesListaItemComponent {
  /**
   * Dados de cada requisição
   */
  @Input() requisicao;
  /**
   * Output sinalizando intenção de atualização
   */
  @Output() atualizar = new EventEmitter();

  emitirAtualizar(requisicao) {
    this.atualizar.emit(requisicao);
  }
}
