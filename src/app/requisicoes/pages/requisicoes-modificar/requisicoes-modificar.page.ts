import { Component, ViewChild } from "@angular/core";
import { RequisicoesFormService } from "./services/requisicoes-form/requisicoes-form.service";
import { FormGroup, FormArray } from "@angular/forms";
import { Exame } from "src/app/exames/models/exames.model";
import { Lista } from "src/app/listas/models/listas.model";
import { ActivatedRoute } from "@angular/router";
import { OverlayService } from "src/app/shared/services/overlay/overlay.service";
import { LaudosService } from "../../services/laudos/laudos.service";
import { AuthService } from "src/app/core/services/auth.service";
import { NavController, PickerController } from "@ionic/angular";

@Component({
  selector: "app-requisicoes-modificar",
  templateUrl: "./requisicoes-modificar.page.html",
  styleUrls: ["./requisicoes-modificar.page.scss"],
  providers: [RequisicoesFormService],
})
export class RequisicoesModificarPage {
  @ViewChild("busca") busca;
  tabelasPrecos;
  listaTabelasPrecos;
  isAdmin;
  usuarioAtual;
  modoEdicao = false;
  aba = "dados";
  requisicaoId;
  requisicao;
  requisicaoCarregada = false;
  erro: string;
  exames: Exame[];
  listaExames;

  lista: Lista;
  examesEscolhidos = [];
  total = 0.0;
  textoReferencia;

  // ====================================================== //
  // ===================== Construtor ===================== //
  // ====================================================== //
  constructor(
    public requisicoesFormService: RequisicoesFormService,
    private route: ActivatedRoute,
    private overlayService: OverlayService,
    private navCtrl: NavController,
    private pickerCtrl: PickerController,
    //
    private laudosService: LaudosService,
    private auth: AuthService //
  ) {}

  // ~~~~~~~~~~~~ Getter do form ~~~~~~~~~~~ //
  get form(): FormGroup {
    return this.requisicoesFormService.form as FormGroup;
  }

  // ====================================================== //
  // ======================= Eventos ====================== //
  // ====================================================== //
  onAba(e) {
    this.requisicoesFormService.selecionarExame(null);
    this.aba = e.detail.value;
  }

  async salvar() {
    const loading = await this.overlayService.loading({
      message: "Salvando...",
    });
    try {
      await this.requisicoesFormService.salvarRequisicao();
      // await this.atualizarExames().then(async () => {
      //   this.requisicoesFormService.salvarRequisicao();
      // });
    } catch (e) {
      console.error(e);
      this.erro = e;
    } finally {
      loading.dismiss();
      this.navCtrl.navigateBack("/requisicoes");
    }
  }

  // ====================================================== //
  // ======================= EXAMES ======================= //
  // ====================================================== //
  adicionarExame(exame, resultadoAnterior?: string) {
    this.requisicoesFormService.adicionarExame(
      exame.id,
      this.form.controls.exames as FormArray,
      this.form.value.especie,
      this.form.value.idade
    );
  }

  selecionarExame(index) {
    this.requisicoesFormService.selecionarExame(index);
  }

  removerExame(i) {
    const examesArray = this.requisicoesFormService.form.get(
      "exames"
    ) as FormArray;
    examesArray.removeAt(i);
  }

  liberarExame(ev) {
    if (ev.target.checked) {
      this.requisicoesFormService.exameSelecionado.patchValue({
        situacao: "Finalizado",
      });
    } else {
      this.requisicoesFormService.exameSelecionado.patchValue({
        situacao: "Aguardando Resultados",
      });
    }
  }

  atualizarReferencias() {
    if (this.form.value.exames) {
      const examesCtrl = this.form.get("exames") as FormArray;
      for (const exameCtrl of examesCtrl.controls) {
        if (
          exameCtrl.value.especie &&
          exameCtrl.value.especie !== this.form.value.especie
        ) {
          this.requisicoesFormService.atualizarExame(
            exameCtrl as FormGroup,
            this.form.value.especie,
            this.form.value.idade
          );
        }
      }
    }
  }

  obterTabelaPrecos(tabelaPreco) {
    this.requisicoesFormService.carregarTabelaDePrecos(tabelaPreco.id);
  }
  // ====================================================== //
  // ====================== Auxiliar ====================== //
  // ====================================================== //
  procurarNoObjeto(object, prop, value) {
    if (!object) return;
    if (object.hasOwnProperty(prop) && object[prop] == value) {
      return object;
    }

    for (let i = 0; i < Object.keys(object).length; i++) {
      if (typeof object[Object.keys(object)[i]] == "object") {
        let o = this.procurarNoObjeto(
          object[Object.keys(object)[i]],
          prop,
          value
        );
        if (o != null) {
          return o;
        }
      }
    }

    return null;
  }

  testarForm() {
    console.log("test");
    console.log(this.form);
    console.log(this.requisicoesFormService.exameSelecionado);
  }

  // ====================================================== //
  // =============== Situação da requisição =============== //
  // ====================================================== //
  liberarResultado(ev) {
    if (ev.target.checked) {
      let data = new Date();
      // data = new Date(data.valueOf() - data.getTimezoneOffset() * 60000);
      let dataString = data.toISOString();
      dataString = data + "-03:00";
      this.form.controls.situacao.setValue("Finalizado");
      this.form.controls.resultado.setValue(dataString);
    } else {
      this.form.controls.situacao.setValue("Aguardando Liberação");
      this.form.controls.resultado.setValue(null);
    }
  }

  receberAmostra(ev) {
    if (ev.target.checked) {
      this.form.controls.situacao.setValue("Aguardando Resultados");
    } else {
      this.form.controls.situacao.setValue("Aguardando Amostras");
    }
  }

  definirUsuarioAssociado(ev) {
    if (ev.email) {
      const usuario = {
        uid: ev.objectID,
        email: ev.email,
      };
      this.form.controls.usuarioAssociado.patchValue(usuario);
    }
  }

  removerRequisicao() {
    // @ts-ignore
    this.requisicoesFormService.removerRequisicao(this.requisicaoId);
    this.navCtrl.navigateBack("/requisicoes");
  }
  // ====================================================== //
  // ======================== Laudo ======================= //
  // ====================================================== //
  gerarLaudo() {
    this.laudosService.gerarPDF(
      JSON.parse(JSON.stringify(this.form.getRawValue()))
    );
    if (this.isAdmin) {
      this.salvar();
    }
  }

  // ====================================================== //
  // ========================= UI ========================= //
  // ====================================================== //
  async openPicker(ev) {
    const dias = [];
    const meses = [];
    const anos = [];
    for (let index = 1; index <= 30; index++) {
      dias.push({
        text: index,
        value: index,
      });
    }
    for (let index = 0; index <= 11; index++) {
      meses.push({
        text: index,
        value: index,
      });
    }
    for (let index = 0; index <= 30; index++) {
      anos.push({
        text: index,
        value: index,
      });
    }
    let picker = await this.pickerCtrl.create({
      buttons: [
        {
          text: "OK",
          handler: (data) => {
            let idade: number = parseFloat(data.anos.value) * 12;
            idade += parseFloat(data.meses.value);
            idade += parseFloat(data.dias.value) / 30;
            ev.target.value = idade.toFixed(1);
          },
        },
      ],
      columns: [
        {
          suffix: "anos",
          name: "anos",
          options: anos,
        },
        {
          suffix: "meses",
          name: "meses",
          options: meses,
        },
        {
          suffix: "dias",
          name: "dias",
          options: dias,
        },
      ],
    });

    await picker.present();
  }

  // ====================================================== //
  // ==================== Ciclo de Vida =================== //
  // ====================================================== //
  async ionViewDidEnter() {
    try {
      this.requisicaoId = this.route.snapshot.paramMap.get("id");
      this.modoEdicao = !!this.requisicaoId;
      this.usuarioAtual = await this.auth.usuarioToken();
      this.isAdmin = await this.auth.isAdmin();
      //
      if (this.modoEdicao) {
        const requisicao = await this.requisicoesFormService.carregarRequisicao(
          this.requisicaoId
        );

        // this.requisicoesFormService.observarSituacao();
      } else {
        console.log(await this.auth.usuarioToken());
        this.form.controls.usuarioAssociado.patchValue({
          email: this.usuarioAtual.email,
          uid: this.usuarioAtual.uid,
        });

        this.form.controls.autor.patchValue({
          nome: this.usuarioAtual.nome,
          uid: this.usuarioAtual.uid,
        });
      }
      if (this.isAdmin) {
        this.form.controls.usuarioAssociado.enable();
      }

      await this.requisicoesFormService.carregarListasTabelaPrecoseEspecies();
      this.requisicoesFormService.observarMudanças();
    } catch (error) {
      console.log(error);
    } finally {
      this.requisicaoCarregada = true;
    }
  }
}
