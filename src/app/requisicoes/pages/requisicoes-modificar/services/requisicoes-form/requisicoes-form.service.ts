import { Injectable, OnDestroy } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  FormArray,
  FormControl,
  Validators,
} from "@angular/forms";
import {
  Requisicao,
  ExameRequisicao,
  CampoRequisicao,
} from "../../../../models/requisicoes.model";
import { AuthService } from "src/app/core/services/auth.service";
import { Subscription } from "rxjs";
import { ListasService } from "src/app/listas/services/listas.service";
import { take } from "rxjs/operators";
import { RequisicoesService } from "src/app/requisicoes/services/requisicoes/requisicoes.service";
import { TabelaPrecosService } from "src/app/tabela-precos/services/tabela-precos.service";
import { Lista } from "src/app/listas/models/listas.model";
import { ExamesService } from "src/app/exames/services/exames/exames.service";
import { Campos } from "src/app/exames/models/exames.model";
import { firestore } from "firebase";
import Timestamp = firestore.Timestamp;

interface Referencia {
  idadeMinima: string;
  idadeMaxima: string;
  valorMinimo: string;
  valorMaximo: string;
}

interface FaixaDeIdade {
  idadeMinima: string;
  idadeMaxima: string;
}

@Injectable()
export class RequisicoesFormService implements OnDestroy {
  public form: FormGroup;
  private modoEdicao = false;
  listaTabelasPrecos: Lista;
  listaEspecies: Lista;
  tabelaPrecos;
  exameSelecionadoIndice: number;
  formMudancas: Subscription;
  usuarioAtual;
  private examesSubs: Subscription = new Subscription();

  // ====================================================== //
  // ======================== CRUD ======================== //
  // ====================================================== //

  async carregarRequisicao(id) {
    this.modoEdicao = true;
    const requisicao = await this.requisicoesService
      .obterPorId(id)
      .pipe(take(1))
      .toPromise();
    this.carregarRequisicaoParaEdicao(requisicao);
    return requisicao;
  }

  salvarRequisicao() {
    const value = this.form.getRawValue();
    value.coleta = value.coleta ? new Date(value.coleta) : "";
    value.resultado = value.resultado ? new Date(value.resultado) : "";
    console.log(value);
    const exame = !this.modoEdicao
      ? this.requisicoesService.criar(value)
      : this.requisicoesService.atualizar({
          id: value.id,
          ...value,
        });
    return exame;
  }

  removerRequisicao(id: string) {
    this.requisicoesService.delete(id);
  }

  // ====================================================== //
  // ====================== SELETORES ===================== //
  // ====================================================== //
  get exames() {
    return this.form.get("exames") as FormArray;
  }

  get campos() {
    return this.exames
      .at(this.exameSelecionadoIndice)
      .get("campos") as FormArray;
  }

  selecionarExame(indice) {
    this.exameSelecionadoIndice = indice;
  }

  get exameSelecionado(): FormGroup {
    return this.exames.at(this.exameSelecionadoIndice) as FormGroup;
  }

  // ====================================================== //
  // ================= LER OUTROS SERVIÇOS ================ //
  // ====================================================== //
  async carregarListasTabelaPrecoseEspecies() {
    this.listaTabelasPrecos = await this.listasService
      .obterPorId("TabelasPrecos")
      .pipe(take(1))
      .toPromise();

    // @ts-ignore
    this.listaEspecies = await this.listasService
      .obterPorId("Espécies")
      .pipe(take(1))
      .toPromise();
  }

  async carregarTabelaDePrecos(id) {
    this.tabelaPrecos = await this.tabelasPrecosService
      .obterPorId(id)
      .pipe(take(1))
      .toPromise();
  }

  async carregarRequisicaoParaEdicao(dados: Requisicao) {
    const formExames = this.form.controls.exames as FormArray;
    if (dados.exames) {
      dados.exames.forEach((exame, indiceExame) => {
        const exameNovo = this.retornarExame();
        formExames.push(exameNovo);
        const formCampos = formExames.controls[indiceExame]["controls"]
          .campos as FormArray;
        if (exame.campos) {
          exame.campos.forEach((campo, indiceCampo) => {
            const campoNovo = this.retornarCampo();
            formCampos.push(campoNovo);
          });
        }
      });
    }
    // @ts-ignore
    dados.coleta = dados.coleta ? dados.coleta.toDate().toISOString() : "";
    // @ts-ignore
    dados.resultado = dados.resultado
      ? //@ts-ignore
        dados.resultado.toDate().toISOString()
      : "";
    this.form.patchValue(dados);
    this.form.markAsPristine();
  }

  // ====================================================== //
  // =================== CRIAR CONTROLES ================== //
  // ====================================================== //
  retornarExame() {
    return this.fb.group({
      id: "",
      nome: "",
      categoria: "",
      especie: "",
      valor: "",
      idadeMinima: "",
      idadeMaxima: "",
      situacao: "Aguardando Amostras",
      campos: this.fb.array([]),
    });
  }

  retornarCampo() {
    return this.fb.group({
      nome: "",
      unidade: "",
      tipoDeDado: "",
      subcategoria: "",
      valorMinimo: "",
      valorMaximo: "",
      idadeMinima: "",
      idadeMaxima: "",
      resultado: ["", Validators.required],
    });
  }

  // ====================================================== //
  // ================== OBSERVAR MUDANÇAS ================= //
  // ====================================================== //

  observarMudanças() {
    this.formMudancas = this.form.valueChanges.subscribe(() => {
      if (this.exameSelecionado) {
        // Calculo do hemograma
        if (this.exameSelecionado.value.nome === "Hemograma") {
          this.calcularHemograma(this.exameSelecionado);
        }
        // Situação do exame
        if (this.exameSelecionado.valid) {
          if (
            this.exameSelecionado.value.situacao === "Aguardando Resultados"
          ) {
            this.exameSelecionado.patchValue(
              { situacao: "Aguardando Liberação" },
              { onlySelf: true }
            );
          }
        } else {
          if (
            this.exameSelecionado.value.situacao !== "Aguardando Resultados"
          ) {
            this.exameSelecionado.patchValue(
              { situacao: "Aguardando Resultados" },
              { onlySelf: true }
            );
          }
        }
      }
      // Situação da requisição
      // INVALIDO
      if (this.form.invalid) {
        if (this.form.value.situacao !== "Aguardando Resultados") {
          this.form.controls.situacao.patchValue("Aguardando Resultados");
        }
        // VALIDO
      } else {
        const algumAguardandoResultado = this.form.value.exames.find(
          (exame) => {
            return exame.situacao === "Aguardando Resultados";
          }
        );
        // ALGUM EXAME SEM COMPLETAR
        if (algumAguardandoResultado) {
          if (this.form.value.situacao !== "Aguardando Resultados") {
            this.form.controls.situacao.patchValue("Aguardando Resultados");
          }
          // TODOS EXAMES COMPLETOS
        } else {
          const algumAguardandoLiberacao = this.form.value.exames.find(
            (exame) => {
              return exame.situacao === "Aguardando Liberação";
            }
          );
          // EXAMES COMPLETOS E FALTANDO LIBERAR
          if (algumAguardandoLiberacao) {
            if (this.form.value.situacao !== "Aguardando Resultados") {
              this.form.controls.situacao.patchValue("Aguardando Resultados");
            }
          } else {
            if (this.form.value.situacao === "Finalizado") {
              return;
            }
            if (this.form.value.situacao !== "Aguardando Liberação") {
              // EXAME COMPLETO
              this.form.controls.situacao.patchValue("Aguardando Liberação");
            }
          }
        }
      }
    });
  }

  // ====================================================== //
  // ======================= EXAMES ======================= //
  // ====================================================== //
  async adicionarExame(
    exameId: string,
    examesArray: FormArray,
    especieDada: string,
    idadeDada: string
  ): Promise<void> {
    const faixaDeIdadeLista: FaixaDeIdade[] = [];
    const exameExistente = this.form.value.exames.find((exameEncontrado) => {
      return exameEncontrado.id === exameId;
    });
    if (exameExistente) {
      return;
    }
    // Procurar o exame no banco de dados
    const exame = await this.examesService
      .obterPorId(exameId)
      .pipe(take(1))
      .toPromise();
    // Controle do exame em branco
    const exameCtrlNovo = this.retornarExame();
    // Modelo do exame;
    const exameModeloNovo: ExameRequisicao = {
      id: exame.id,
      nome: exame.nome,
      categoria: exame.categoria,
      valor: exame.valor,
      especie: especieDada,
      situacao: "Aguardando Resultados",
      idadeMinima: "",
      idadeMaxima: "",
      campos: [],
    };
    // Procurar os valores de referencia
    // Baseado na espécie e faixa de idade
    for (const campo of exame.campos) {
      // Controle do campo em branco
      const campoCtrlNovo = this.retornarCampo();
      // Dados da referencia
      const referencia = this.obterReferencia(campo, especieDada, idadeDada);
      // Modelo de campo
      const valorPadrao = campo.valorPadrao;
      delete campo.especies, delete campo.ordem, delete campo.valorPadrao;
      const campoModeloNovo: CampoRequisicao = {
        resultado: valorPadrao || "",
        ...campo,
        ...referencia,
      };
      // Adicionar o campo no modelo do exame
      exameModeloNovo.campos.push(campoModeloNovo);
      // Adicionar um campo em branco no controle do exame
      const campoArray = exameCtrlNovo.controls.campos as FormArray;
      campoArray.push(campoCtrlNovo);
      faixaDeIdadeLista.push({
        idadeMinima: campoModeloNovo.idadeMinima,
        idadeMaxima: campoModeloNovo.idadeMaxima,
      });
    }
    // Obter a faixa de idade do exame, considerando todos campos
    const faixaDeIdade = this.obterFaixaDeIdadeExame(faixaDeIdadeLista);
    Object.assign(exameModeloNovo, { ...faixaDeIdade });
    exameCtrlNovo.setValue(exameModeloNovo);
    examesArray.push(exameCtrlNovo);
    // Renovar observer de mudanças para abranger os novos controles
    this.formMudancas.unsubscribe();
    this.observarMudanças();
  }

  async atualizarExame(
    exameCtrl: FormGroup,
    especieDada: string,
    idadeDada: string
  ) {
    const faixaDeIdadeLista: FaixaDeIdade[] = [];

    // Valores do controle
    const exameModelo: ExameRequisicao = exameCtrl.value;
    // Procurar o exame no banco de dados
    const exame = await this.examesService
      .obterPorId(exameCtrl.value.id)
      .pipe(take(1))
      .toPromise();

    // Procurar os valores de referencia
    // Baseado na espécie e faixa de idade
    for (const campo of exame.campos) {
      // Dados da referencia
      const referencia = this.obterReferencia(campo, especieDada, idadeDada);
      // Modelo de campo
      const campoEncontrado = exameModelo.campos.find((campoDado) => {
        return campo.nome === campoDado.nome;
      });
      Object.assign(campoEncontrado, referencia);
      faixaDeIdadeLista.push({
        idadeMinima: campoEncontrado.idadeMinima,
        idadeMaxima: campoEncontrado.idadeMaxima,
      });
    }
    // Obter a faixa de idade do exame, considerando todos campos
    const faixaDeIdade = this.obterFaixaDeIdadeExame(faixaDeIdadeLista);
    Object.assign(exameModelo, { ...faixaDeIdade });
    // Obter a espécie nova
    exameModelo.especie = especieDada;
    // Atribuir os novos dados ao controle dado
    exameCtrl.setValue(exameModelo);
  }

  private obterReferencia(
    campo: Campos,
    especieDada: string,
    idadeDada: string
  ): Referencia {
    const especieRef = campo.especies.find((especie) => {
      return especie.nome === especieDada;
    });
    if (!especieRef) {
      return {
        idadeMinima: "",
        idadeMaxima: "",
        valorMinimo: "",
        valorMaximo: "",
      };
    }
    for (const faixaIdade of especieRef.referencias) {
      let idadeMaxima = faixaIdade.idadeMaxima;
      if (idadeMaxima === "*") {
        idadeMaxima = "9999";
      }
      if (
        parseFloat(idadeDada) >= parseFloat(faixaIdade.idadeMinima) &&
        parseFloat(idadeDada) < parseFloat(idadeMaxima)
      ) {
        return {
          idadeMinima: faixaIdade.idadeMinima,
          idadeMaxima: faixaIdade.idadeMaxima,
          valorMinimo: faixaIdade.valorMinimo,
          valorMaximo: faixaIdade.valorMaximo,
        };
      }
    }
  }

  private obterFaixaDeIdadeExame(faixaDeIdadeLista: FaixaDeIdade[]) {
    let intervaloMinimo = Infinity;
    let faixaSelecionada: FaixaDeIdade = {
      idadeMinima: "0",
      idadeMaxima: "*",
    };
    for (const faixaDeIdade of faixaDeIdadeLista) {
      const idadeMinima = parseFloat(faixaDeIdade.idadeMinima);
      const idadeMaxima =
        faixaDeIdade.idadeMaxima === "*"
          ? 9999
          : parseFloat(faixaDeIdade.idadeMaxima);
      const intervalo = idadeMaxima - idadeMinima;
      if (intervalo < intervaloMinimo) {
        intervaloMinimo = intervalo;
        faixaSelecionada = faixaDeIdade;
      }
    }
    return faixaSelecionada;
  }

  calcularHemograma(hemogramaCtrl: FormGroup) {
    const hemogramaCtrlModel = hemogramaCtrl.value;
    const exames = hemogramaCtrlModel.campos.reduce((acum, curr, i) => {
      if (curr.tipoDeDado === "número" || curr.tipoDeDado === "fórmula") {
        const campo = curr.nome;
        if (typeof curr.resultado === "string") {
          curr.resultado = curr.resultado.replace(",", ".");
        }
        acum[campo] = { indice: i, valor: parseFloat(curr.resultado) };
      }
      return acum;
    }, {});

    // VCM
    hemogramaCtrlModel.campos[exames.VCM.indice].resultado =
      (exames.Hematócrito.valor * 10) / exames.Hemácias.valor;
    // CHCM
    hemogramaCtrlModel.campos[exames.CHCM.indice].resultado =
      (exames.Hemoglobina.valor * 100) / exames.Hematócrito.valor;
    // Mielócitos
    hemogramaCtrlModel.campos[exames["Mielócitos #abs"].indice].resultado =
      (exames["Mielócitos #rel"].valor * exames.Leucócitos.valor) / 100;
    // Metamielócitos
    hemogramaCtrlModel.campos[exames["Metamielócitos #abs"].indice].resultado =
      (exames["Metamielócitos #rel"].valor * exames.Leucócitos.valor) / 100;
    // Bastonetes
    hemogramaCtrlModel.campos[exames["Bastonetes #abs"].indice].resultado =
      (exames["Bastonetes #rel"].valor * exames.Leucócitos.valor) / 100;
    // Segmentados
    hemogramaCtrlModel.campos[exames["Segmentados #abs"].indice].resultado =
      (exames["Segmentados #rel"].valor * exames.Leucócitos.valor) / 100;
    // Eosinófilos
    hemogramaCtrlModel.campos[exames["Eosinófilos #abs"].indice].resultado =
      (exames["Eosinófilos #rel"].valor * exames.Leucócitos.valor) / 100;
    // Basófilos
    hemogramaCtrlModel.campos[exames["Basófilos #abs"].indice].resultado =
      (exames["Basófilos #rel"].valor * exames.Leucócitos.valor) / 100;
    // Linfócitos
    hemogramaCtrlModel.campos[exames["Linfócitos #abs"].indice].resultado =
      (exames["Linfócitos #rel"].valor * exames.Leucócitos.valor) / 100;
    // Monócitos
    hemogramaCtrlModel.campos[exames["Monócitos #abs"].indice].resultado =
      (exames["Monócitos #rel"].valor * exames.Leucócitos.valor) / 100;

    for (const campo of hemogramaCtrlModel.campos) {
      if (campo.tipoDeDado === "número" || campo.tipoDeDado === "fórmula") {
        if (typeof campo.resultado === "number") {
          campo.resultado = parseFloat(campo.resultado.toFixed(2));
          campo.resultado = campo.resultado.toString();
          console.log(campo.resultado);
        }
        campo.resultado = campo.resultado.replace(".", ",");
      }
    }
    hemogramaCtrl.setValue(hemogramaCtrlModel, { emitEvent: false });

    if (
      exames["Mielócitos #rel"].valor +
        exames["Metamielócitos #rel"].valor +
        exames["Bastonetes #rel"].valor +
        exames["Segmentados #rel"].valor +
        exames["Eosinófilos #rel"].valor +
        exames["Basófilos #rel"].valor +
        exames["Linfócitos #rel"].valor +
        exames["Monócitos #rel"].valor !==
      100
    ) {
      hemogramaCtrl.setErrors({ menorQue100: true });
    }
  }
  // ====================================================== //
  // ===================== CONSTRUTOR ===================== //
  // ====================================================== //
  constructor(
    private fb: FormBuilder,
    private auth: AuthService,
    private listasService: ListasService,
    private tabelasPrecosService: TabelaPrecosService,
    public requisicoesService: RequisicoesService,
    private examesService: ExamesService
  ) {
    let usuario;
    this.auth.authState$.subscribe((res) => {
      usuario = {
        uid: res.uid,
        email: res.email,
      };
    });

    this.form = this.fb.group({
      id: "",
      nomeAnimal: "",
      especie: "",
      raca: "",
      sexo: "",
      idade: "",
      tutor: "",
      veterinarioSolicitante: "",
      clinicaSolicitante: "",
      coleta: "",
      resultado: [{ value: "", disabled: true }],
      observacoes: "",
      situacao: "",
      autor: this.fb.group({
        uid: [{ value: "", disabled: true }],
        email: [{ value: "", disabled: true }],
      }),
      usuarioAssociado: this.fb.group({
        uid: [{ value: "", disabled: true }],
        email: [{ value: "", disabled: true }],
      }),
      tabelaPrecos: "",
      exames: this.fb.array([]),
    });
  }

  // ====================================================== //
  // ==================== CICLO DE VIDA =================== //
  // ====================================================== //
  ngOnDestroy() {
    // Desescrever se tiver algum inscrito
    this.examesSubs.unsubscribe();
  }
}
