import { TestBed } from '@angular/core/testing';

import { RequisicoesFormService } from './requisicoes-form.service';
import { RequisicoesService } from 'src/app/requisicoes/services/requisicoes/requisicoes.service';
import { Observable } from 'rxjs';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthService } from 'src/app/core/services/auth.service';
import { ListasService } from 'src/app/listas/services/listas.service';
import { TabelaPrecosService } from 'src/app/tabela-precos/services/tabela-precos.service';
import { ExamesService } from 'src/app/exames/services/exames/exames.service';

const authServiceStub = {
  isAdmin: () => {
    return true;
  },
  usuarioToken: () => {
    return {
      nome: 'Admin',
      uid: '123'
    };
  },
  authState$: {
    subscribe: () => {
      return new Observable(subscriber => {
        subscriber.next({
          nomeAnimal: 'animalNome'
        });
      });
    }
  }
};
const requisicoesServiceStub = {
  obterPorId: () => {
    return new Observable(subscriber => {
      subscriber.next({
        nomeAnimal: 'animalNome'
      });
    });
  }
};
const listasServiceStub = {
  obterPorId: () => {
    return new Observable(subscriber => {
      subscriber.next({
        nomeAnimal: 'animalNome'
      });
    });
  }
};
const tabelaPrecosServiceStub = {
  obterPorId: () => {
    return new Observable(subscriber => {
      subscriber.next({
        nomeAnimal: 'animalNome'
      });
    });
  }
};
const examesServiceStub = {
  obterPorId: () => {
    return new Observable(subscriber => {
      subscriber.next({
        nomeAnimal: 'animalNome'
      });
    });
  }
};
describe('RequisicoesFormService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      providers: [
        { provide: AuthService, useValue: authServiceStub },
        { provide: ListasService, useValue: listasServiceStub },
        { provide: TabelaPrecosService, useValue: tabelaPrecosServiceStub },
        { provide: RequisicoesService, useValue: requisicoesServiceStub },
        { provide: ExamesService, useValue: examesServiceStub },

        RequisicoesFormService
      ]
    })
  );

  it('should be created', () => {
    const service: RequisicoesFormService = TestBed.get(RequisicoesFormService);
    expect(service).toBeTruthy();
  });
});
