import { NgModule } from '@angular/core';
import { RequisicoesModificarPage } from './requisicoes-modificar.page';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
const routes: Routes = [
  {
    path: '',
    component: RequisicoesModificarPage
  }
];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [RequisicoesModificarPage]
})
export class RequisicoesModificarPageModule {}
