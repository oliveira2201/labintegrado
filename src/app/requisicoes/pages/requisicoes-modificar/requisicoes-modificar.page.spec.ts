import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { RequisicoesModificarPage } from './requisicoes-modificar.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuthService } from 'src/app/core/services/auth.service';
import { Observable } from 'rxjs';
import { RequisicoesFormService } from './services/requisicoes-form/requisicoes-form.service';
import { ExamesService } from 'src/app/exames/services/exames/exames.service';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { TabelaPrecosService } from 'src/app/tabela-precos/services/tabela-precos.service';

describe('RequisicoesModificarPage', () => {
  let component: RequisicoesModificarPage;
  let fixture: ComponentFixture<RequisicoesModificarPage>;
  const authServiceStub = {
    isAdmin: () => {
      return true;
    },
    usuarioToken: () => {
      return {
        nome: 'Admin',
        uid: '123'
      };
    }
  };
  const requisicoesServiceStub = {
    obterPorId: () => {
      return new Observable(subscriber => {
        subscriber.next({
          nomeAnimal: 'animalNome'
        });
      });
    }
  };
  const requisicoesFormServiceStub = {
    observarSituacao: () => {}
  };
  const routeStub = {
    snapshot: {
      paramMap: {
        get: () => {
          return '123';
        }
      }
    }
  };
  const examesServiceStub = {
    obterPorId: () => {
      return new Observable(subscriber => {
        subscriber.next({
          nomeAnimal: 'animalNome'
        });
      });
    }
  };
  const tabelaPrecosServiceStub = {
    obterPorId: () => {
      return new Observable(subscriber => {
        subscriber.next({
          nomeAnimal: 'animalNome'
        });
      });
    }
  };

  beforeEach(async(() => {
    TestBed.overrideComponent(RequisicoesModificarPage, {
      set: {
        providers: [{ provide: RequisicoesFormService, useValue: requisicoesFormServiceStub }]
      }
    });
    TestBed.configureTestingModule({
      declarations: [RequisicoesModificarPage],
      imports: [IonicModule.forRoot(), SharedModule, RouterModule.forRoot([])],
      providers: [
        { provide: ActivatedRoute, useValue: routeStub },
        //
        { provide: AuthService, useValue: authServiceStub },
        { provide: ExamesService, useValue: examesServiceStub },
        { provide: TabelaPrecosService, useValue: tabelaPrecosServiceStub }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(RequisicoesModificarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('deve ser criado', () => {
    expect(component).toBeTruthy();
  });
});
