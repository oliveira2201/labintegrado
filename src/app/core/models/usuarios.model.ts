export interface Usuario {
  uid: string;
  email: string;
  nome: string;
  telefone: string;
}
