import { NgModule } from '@angular/core';
import { LoginPage } from './login.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { UsuarioCadastroComponent } from '../../components/usuario-cadastro/usuario-cadastro.component';

const routes: Routes = [
  {
    path: '',
    component: LoginPage
  }
];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [LoginPage, UsuarioCadastroComponent]
})
export class LoginPageModule {}
