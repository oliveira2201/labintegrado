import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LoginPage } from './login.page';
import { UsuarioCadastroComponent } from '../../components/usuario-cadastro/usuario-cadastro.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { Observable } from 'rxjs';
import { AuthService } from '../../services/auth.service';
import { RouterModule } from '@angular/router';

const authServiceStub = {
  isAdmin: () => {
    return true;
  },
  usuarioToken: () => {
    return {
      nome: 'Admin',
      uid: '123'
    };
  },
  authState$: {
    subscribe: () => {
      return new Observable(subscriber => {
        subscriber.next({
          nomeAnimal: 'animalNome'
        });
      });
    }
  }
};

describe('LoginPage', () => {
  let component: LoginPage;
  let fixture: ComponentFixture<LoginPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: AuthService, useValue: authServiceStub }],
      declarations: [LoginPage, UsuarioCadastroComponent],
      imports: [IonicModule.forRoot(), SharedModule, RouterModule.forRoot([])]
    }).compileComponents();

    fixture = TestBed.createComponent(LoginPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('deve ser criado', () => {
    expect(component).toBeTruthy();
  });
});
