import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, ValidatorFn, FormGroup, ValidationErrors } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { OverlayService } from 'src/app/shared/services/overlay/overlay.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {
  erroTraduzido: string;
  listaErros: string[];
  animacao;
  status = 'login';
  cadastroForm = this.fb.group(
    {
      nome: ['', Validators.required],
      telefone: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      senha: ['', [Validators.required, Validators.minLength(6)]],
      confirmarSenha: ['']
    },
    { validators: this.validarSenha }
  );

  loginForm = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    senha: ['', [Validators.required, Validators.minLength(6)]]
  });

  async cadastrarUsuario() {
    try {
      this.listaErros = [];
      await this.auth.cadastrarUsuario(this.cadastroForm.value);
      this.navCtrl.navigateForward('/requisicoes');
    } catch (error) {
      this.erro = error;
      this.animacao = 'fadeIn';
      this.listaErros.push(this.erro);
    }
  }

  async login() {
    const loading = await this.overlayService.loading();
    try {
      this.listaErros = [];
      await this.auth.login({ ...this.loginForm.value });
      this.navCtrl.navigateForward('/requisicoes');
    } catch (error) {
      this.erro = error;
      this.animacao = 'fadeIn';
      this.listaErros.push(this.erro);
    } finally {
      loading.dismiss();
    }
  }

  // Tradução dos erros mais comuns
  set erro(e) {
    // @ts-ignore
    switch (e.message) {
      case 'The password is invalid or the user does not have a password.':
        this.erroTraduzido = 'Usuário ou senha inválidos';
        break;
      case 'Too many unsuccessful login attempts. Please include reCaptcha verification or try again later.':
        this.erroTraduzido = 'Muitas tentativas de login. Tente novamente mais tarde';
        break;
      case 'The email address is already in use by another account.':
        this.erroTraduzido = 'Email já cadastrado';
        break;
      case 'There is no user record corresponding to this identifier. The user may have been deleted.':
        this.erroTraduzido = 'Usuário ou senha inválidos';
        break;
      case 'The email address is badly formatted.':
        this.erroTraduzido = 'Email com formato inválido';
        break;
      default:
        this.erroTraduzido = e;
        break;
    }
  }
  get erro() {
    return this.erroTraduzido;
  }

  validarSenha(controle: FormGroup): ValidationErrors | null {
    const senha = controle.get('senha');
    const confirmarSenha = controle.get('confirmarSenha');
    const senhasIguais = senha.value === confirmarSenha.value;
    if (!senhasIguais) {
      confirmarSenha.setErrors({ senhasDiferentes: true });
      return { senhasDiferentes: true };
    }
    confirmarSenha.setErrors(null);
    return null;
  }

  constructor(
    private fb: FormBuilder,
    private auth: AuthService,
    private overlayService: OverlayService,
    private navCtrl: NavController
  ) {}

  ngOnInit() {}
}
