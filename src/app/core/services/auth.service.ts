import { Injectable } from "@angular/core";
import { switchMap, first, map, tap } from "rxjs/operators";
import { Usuario } from "../models/usuarios.model";
import { Observable, of } from "rxjs";
import { AngularFireAuth } from "@angular/fire/auth";
import {
  AngularFirestore,
  AngularFirestoreDocument,
} from "@angular/fire/firestore";
import { Router } from "@angular/router";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  // usuario: Observable<Usuario>;
  authState$: Observable<firebase.User>;

  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router
  ) {
    this.authState$ = this.afAuth.authState;

    //// Get auth data, then get firestore user document || null
    // this.usuario = this.afAuth.authState.pipe(
    //   switchMap(usuario => {
    //     if (usuario) {
    //       return this.afs
    //         .doc<Usuario>(`usuarios/${usuario.uid}`)
    //         .valueChanges();
    //     } else {
    //       return of(null);
    //     }
    //   })
    // );
  }

  async usuarioToken() {
    const usuario = await this.authState$.pipe(first()).toPromise();
    // @ts-ignore
    const claims = await usuario.getIdTokenResult().claims;
    return {
      email: usuario.email,
      uid: usuario.uid,
      claims,
    };
  }

  login({ email, senha }) {
    return this.afAuth.auth.signInWithEmailAndPassword(email, senha);
  }

  cadastrarUsuario(usuario) {
    return this.afAuth.auth
      .createUserWithEmailAndPassword(usuario.email, usuario.senha)
      .then((user) => {
        user.user.updateProfile({ displayName: usuario.nome });
        this.atualizarDadosUsuario(user.user, usuario);
      });
  }

  private atualizarDadosUsuario(credencial, usuario) {
    // Sets user data to firestore on login
    const usuarioRef: AngularFirestoreDocument<any> = this.afs.doc(
      `usuarios/${credencial.uid}`
    );

    const data: Usuario = {
      uid: credencial.uid,
      email: credencial.email,
      nome: usuario.nome,
      telefone: usuario.telefone,
    };

    return usuarioRef.set(data, { merge: true });
  }

  logout() {
    this.afAuth.auth.signOut().then(() => {
      this.router.navigate(["/"]);
    });
  }

  async obterTokenClaim() {
    const claim = this.afAuth.auth.currentUser
      .getIdTokenResult()
      .then((token) => {
        return token.claims;
      });
    return claim;
  }

  async isAdmin() {
    const claim = await this.obterTokenClaim();
    return !!claim.admin;
  }
}
