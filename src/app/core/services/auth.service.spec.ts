import { TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';

const angularFireAuthStub = {
  isAdmin: () => {
    return true;
  }
};

const angularFirestoreStub = {
  isAdmin: () => {
    return true;
  }
};

const routerStub = {
  isAdmin: () => {
    return true;
  }
};

describe('AuthService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [
        { provide: AngularFireAuth, useValue: angularFireAuthStub },
        { provide: AngularFirestore, useValue: angularFirestoreStub },
        { provide: Router, useValue: routerStub }
      ]
    })
  );

  it('deve ser criado', () => {
    const service: AuthService = TestBed.get(AuthService);
    expect(service).toBeTruthy();
  });
});
