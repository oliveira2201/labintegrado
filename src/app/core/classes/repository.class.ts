import { AngularFirestore, AngularFirestoreCollection, QueryFn } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

export abstract class Repository<T extends { id: string }> {
  protected collection: AngularFirestoreCollection<T>;
  constructor(protected db: AngularFirestore) {}

  protected setCollection(path: string, queryFn?: QueryFn): void {
    this.collection = path ? this.db.collection(path, queryFn) : null;
  }

  obterTodos(): Observable<T[]> {
    return this.collection.valueChanges();
  }

  obterPorId(id: string): Observable<T> {
    return this.collection.doc<T>(id).valueChanges();
  }

  protected setItem(item: T, operacao: 'set' | 'update'): Promise<T> {
    return this.collection
      .doc<T>(item.id)
      [operacao](item)
      .then(() => item);
  }

  criar(item: T, id: string = undefined): void {
    id === undefined ? (item.id = this.db.createId()) : (item.id = id);
    this.setItem(item, 'set');
  }

  atualizar(item: T): Promise<T> {
    return this.setItem(item, 'update');
  }

  delete(item: T | string): Promise<void> {
    if (typeof item === 'object') {
      return this.collection.doc<T>(item.id).delete();
    }
    return this.collection.doc<T>(item).delete();
  }
}
