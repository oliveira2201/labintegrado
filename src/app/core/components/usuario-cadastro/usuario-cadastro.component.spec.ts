import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UsuarioCadastroComponent } from './usuario-cadastro.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormBuilder, FormGroup } from '@angular/forms';

describe('UsuarioCadastroComponent', () => {
  let component: UsuarioCadastroComponent;
  let fixture: ComponentFixture<UsuarioCadastroComponent>;
  const fb: FormBuilder = new FormBuilder();
  const controle: FormGroup = fb.group({ email: '', senha: '' });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UsuarioCadastroComponent],
      imports: [IonicModule.forRoot(), SharedModule]
    }).compileComponents();

    fixture = TestBed.createComponent(UsuarioCadastroComponent);
    component = fixture.componentInstance;
    component.formGrupoLogin = controle;
    component.formGrupoCadastro = controle;
    fixture.detectChanges();
  }));

  it('deve ser criado', () => {
    expect(component).toBeTruthy();
  });
});
