import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-usuario-cadastro',
  templateUrl: './usuario-cadastro.component.html',
  styleUrls: ['./usuario-cadastro.component.scss']
})
export class UsuarioCadastroComponent implements OnInit {
  estado;
  titulo;
  formGrupo;
  @Input() formGrupoCadastro: FormGroup;
  @Input() formGrupoLogin: FormGroup;
  @Output() cadastro = new EventEmitter();
  @Output() login = new EventEmitter();
  constructor() {}

  alterarEstado() {
    if (this.estado === 'login') {
      this.estado = 'cadastro';
      this.titulo = 'Cadastre no sistema';
      this.formGrupo = this.formGrupoCadastro;
    } else {
      this.estado = 'login';
      this.titulo = 'Entre no sistema';
      this.formGrupo = this.formGrupoLogin;
    }
  }

  cadastrarUsuario() {
    this.cadastro.emit();
  }

  realizarLogin() {
    this.login.emit();
  }

  ngOnInit() {
    this.estado = 'login';
    this.titulo = 'Entre no sistema';
    this.formGrupo = this.formGrupoLogin;
  }
}
