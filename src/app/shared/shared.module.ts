import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListaBotoesComponent } from './components/lista-botoes/lista-botoes.component';
import { CardCategoriasComponent } from './components/card-categorias/card-categorias.component';
import { IonicModule } from '@ionic/angular';
import { InputFormComponent } from './components/input-form/input-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ViewContainerComponent } from './components/view-container/view-container.component';
import { ListaPopoverComponent } from './components/busca/components/lista-popover/lista-popover.component';
import { IonicSelectableModule } from 'ionic-selectable';
import { InputBuscaModule } from './components/busca/busca.module';
import { InputBuscaComponent } from './components/busca/components/input-busca/input-busca.component';
import { BrMaskerModule } from 'br-mask';

@NgModule({
  declarations: [
    ListaBotoesComponent,
    CardCategoriasComponent,
    InputFormComponent,
    ViewContainerComponent
  ],
  exports: [
    CommonModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    //
    ListaBotoesComponent,
    CardCategoriasComponent,
    InputFormComponent,
    ViewContainerComponent,
    IonicSelectableModule,
    InputBuscaComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    IonicSelectableModule,
    InputBuscaModule,
    BrMaskerModule
  ],
  entryComponents: [ListaPopoverComponent]
})
export class SharedModule {}
