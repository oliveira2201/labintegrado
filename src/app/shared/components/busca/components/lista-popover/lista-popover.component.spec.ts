import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule, ModalController } from '@ionic/angular';

import { ListaPopoverComponent } from './lista-popover.component';

describe('ListaPopoverComponent', () => {
  let component: ListaPopoverComponent;
  let fixture: ComponentFixture<ListaPopoverComponent>;
  let viewCtrlSpy: jasmine.SpyObj<ModalController>;

  beforeEach(async(() => {
    // Criar spy do serviço
    viewCtrlSpy = jasmine.createSpyObj('ServiceInjected', ['dismiss']);

    TestBed.configureTestingModule({
      declarations: [ListaPopoverComponent],
      imports: [IonicModule.forRoot()],
      providers: [{ provide: ModalController, useValue: viewCtrlSpy }]
    }).compileComponents();

    fixture = TestBed.createComponent(ListaPopoverComponent);
    component = fixture.componentInstance;

    // Inicar com uma lista
    component.listaString = ['abacaxi', 'abacate', 'acerola', 'morango'];
    fixture.detectChanges();
  }));

  it('deve ser criado', () => {
    expect(component).toBeTruthy();
  });

  describe('Se dado um array de strings', () => {
    it('deve filtrá-lo baseado no valor digitado na input', () => {
      // Iniciar a lista
      component.listaString = ['abacaxi', 'abacate', 'acerola', 'morango'];
      component.ionViewDidEnter();
      // Evento de change
      component.onInputChange({ target: { value: 'aba' } });
      fixture.detectChanges();

      expect(component.listaFiltrada).toEqual(['abacaxi', 'abacate']);
    });
    it('deve retornar uma array com os dados filtrados', () => {
      // Iniciar a lista
      component.listaString = ['abacaxi', 'abacate', 'acerola', 'morango'];
      viewCtrlSpy.dismiss.and.returnValue(Promise.resolve(true));

      component.ionViewDidEnter();
      // Evento de change
      component.onInputChange({ target: { value: 'aba' } });
      fixture.detectChanges();

      component.onItemClick(component.listaString[0]);
      expect(viewCtrlSpy.dismiss).toHaveBeenCalledWith(component.listaString[0]);
    });
    it('deve mostrar os dados filtrados em uma lista', () => {
      component.listaString = ['abacaxi', 'abacate', 'acerola', 'morango'];
      viewCtrlSpy.dismiss.and.returnValue(Promise.resolve(true));

      component.ionViewDidEnter();
      // Evento de change
      component.onInputChange({ target: { value: 'aba' } });
      fixture.detectChanges();

      const html: HTMLElement = fixture.nativeElement;
      const lista = html.querySelectorAll('ion-label');
      const listaElemento = [];
      for (const elemento of Object.values(lista)) {
        listaElemento.push(elemento.innerHTML);
      }
      expect(listaElemento).toEqual(['abacaxi', 'abacate']);
    });
  });

  describe('Se dado um array de objetos', () => {
    it('deve filtrá-lo baseado no valor digitado na input', () => {
      // Iniciar a lista
      component.listaObjeto = [
        { id: '1', nome: 'abacaxi' },
        { id: '2', nome: 'abacate' },
        { id: '3', nome: 'acerola' },
        { id: '4', nome: 'morango' }
      ];
      component.propriedadeBuscada = 'nome';
      component.ionViewDidEnter();
      // Evento de change
      component.onInputChange({ target: { value: 'aba' } });
      fixture.detectChanges();

      expect(component.listaFiltrada).toEqual([
        { id: '1', nome: 'abacaxi' },
        { id: '2', nome: 'abacate' }
      ]);
    });
    it('deve retornar uma array com os dados filtrados', () => {
      // Iniciar a lista
      component.listaObjeto = [
        { id: '1', nome: 'abacaxi' },
        { id: '2', nome: 'abacate' },
        { id: '3', nome: 'acerola' },
        { id: '4', nome: 'morango' }
      ];
      component.propriedadeBuscada = 'nome';
      viewCtrlSpy.dismiss.and.returnValue(Promise.resolve(true));

      component.ionViewDidEnter();
      // Evento de change
      component.onInputChange({ target: { value: 'aba' } });
      fixture.detectChanges();

      component.onItemClick(component.listaObjeto[0]);
      expect(viewCtrlSpy.dismiss).toHaveBeenCalledWith(component.listaObjeto[0]);
    });
    it('deve mostrar os dados filtrados em uma lista', () => {
      component.listaObjeto = [
        { id: '1', nome: 'abacaxi' },
        { id: '2', nome: 'abacate' },
        { id: '3', nome: 'acerola' },
        { id: '4', nome: 'morango' }
      ];
      component.propriedadeBuscada = 'nome';

      component.ionViewDidEnter();
      // Evento de change
      component.onInputChange({ target: { value: 'aba' } });
      fixture.detectChanges();

      const html: HTMLElement = fixture.nativeElement;
      const lista = html.querySelectorAll('ion-label');
      const listaElemento = [];
      for (const elemento of Object.values(lista)) {
        listaElemento.push(elemento.innerHTML);
      }
      expect(listaElemento.includes('abacaxi')).toBeTruthy();
    });
  });

  describe('Se dado um indice', () => {
    it('deve utilizar a busca do algolia', () => {
      // Iniciar a lista
      component.indice = 'prod_USUARIOS';
      component.propriedadeBuscada = 'nome';
      component.listaString = null;
      component.ionViewDidEnter();
      // Spy
      // @ts-ignore
      spyOn(component.index, 'search').and.returnValue(Promise.resolve(true));

      // Evento de change
      component.onInputChange({ target: { value: 'aba' } });
      fixture.detectChanges();

      expect(component.index.search).toHaveBeenCalled();
    });
  });
});
