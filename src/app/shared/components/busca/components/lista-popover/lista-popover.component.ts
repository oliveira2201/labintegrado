import { Component, OnInit, Input, ViewChild, OnDestroy } from '@angular/core';
import { ModalController } from '@ionic/angular';
import * as algoliasearch from 'algoliasearch';
import { Index } from 'algoliasearch';

/**
 * Modal para realizar buscas.
 * Apresenta uma caixa de busca e uma lista com os resultados.
 * Pesquisa por índices no algolia, array com string e array com objeto.
 * Retorna uma array com os itens filtrados.
 * Obrigatório: listaString: string[] || indice: string || listaObjeto: objeto[].
 * caso objeto: Obrigatório: propriedadeBuscada: string
 * Opcional: obrigatorio: boolean.
 */
@Component({
  selector: 'app-lista-popover',
  templateUrl: './lista-popover.component.html',
  styleUrls: ['./lista-popover.component.scss']
})
export class ListaPopoverComponent {
  /**
   * Referencia da input, para ser focada ao iniciar
   */
  @ViewChild('input') input;
  /**
   * Armazena a função da forma de pesquisa escolhida ao iniciar
   */
  funcaoPesquisa;
  /**
   * Instancia do índice do algolia
   */
  index: Index;
  /**
   * Lista contendo os dados retornados
   */
  listaFiltrada = [];
  /**
   * Array com as strings a serem pesquisadas
   */
  @Input() listaString: string[];
  /**
   * Array com os objetos a ser pesquisado
   */
  @Input() listaObjeto: object[];
  /**
   * Propriedade do objeto a ser pesquisada
   */
  @Input() propriedadeBuscada: string;
  /**
   * Nome do índice do algolia a ser pesquisado
   */
  @Input() indice: string;
  /**
   * Se obrigatório, não apresenta o botão de OK.
   * Sai somente quando clicar em alguma opção.
   */
  @Input() obrigatorio: boolean;

  // ====================================================== //
  // ===================== Construtor ===================== //
  // ====================================================== //
  constructor(private viewCtrl: ModalController) {}

  // ====================================================== //
  // ======================= Eventos ====================== //
  // ====================================================== //

  /**
   * Chama a funcao da pesquisa escolhida
   * @param ev Componente que realizou a ação
   */
  onInputChange(ev) {
    // Baseado na pesquisa escolhida
    this.funcaoPesquisa(ev.target.value);
  }

  /**
   * Realiza o dismiss o retorna o item escolhido
   * @param item Item escolhido
   * @returns Item escolhido
   */
  onItemClick(item) {
    this.viewCtrl.dismiss(item);
  }

  /**
   * Realiza o dismiss e retorna o texto digitado
   * @returns Texto digitado
   */
  onButtonClick() {
    this.viewCtrl.dismiss(this.input.value);
  }

  // ====================================================== //
  // ================== Funções de Busca ================== //
  // ====================================================== //
  /**
   * Filtra a array com includes
   * @param texto Texto digitado
   */
  procurarNaListaDeString(texto) {
    this.listaFiltrada = this.listaString.filter((item: string) => {
      return item.toLowerCase().includes(texto.toLowerCase());
    });
  }
  procurarNaListaDeObjeto(texto) {
    this.listaFiltrada = this.listaObjeto.filter((item: object) => {
      return item[this.propriedadeBuscada].toLowerCase().includes(texto.toLowerCase());
    });
  }

  /**
   * Filtra a array pelo indico do algolia dado
   * @param texto Texto digitado
   */
  procurarNoIndice(texto) {
    this.index
      .search({
        query: texto,
        attributesToRetrieve: ['nome']
      })
      .then(data => {
        this.listaFiltrada = data.hits;
      });
  }

  // ====================================================== //
  // ==================== Ciclo de vida =================== //
  // ====================================================== //
  /**
   * Inicializa o componente
   */
  ionViewDidEnter() {
    if (this.indice) {
      if (!this.propriedadeBuscada) {
        throw new Error('Propriedade buscada é requerida');
      }
      const client = algoliasearch('N3RZELEE4S', '81bd77f9de5b780c17deea6602807257', {
        protocol: 'https:'
      });
      this.index = client.initIndex(this.indice);
      // Escolhe a pesquisa no indice
      this.funcaoPesquisa = this.procurarNoIndice;
    }
    if (this.listaString) {
      // Escolhe a pesquisa na lista
      this.funcaoPesquisa = this.procurarNaListaDeString;
      // Inicia a lista sem filtros
      this.funcaoPesquisa('');
    }
    if (this.listaObjeto) {
      if (!this.propriedadeBuscada) {
        throw new Error('Propriedade buscada é requerida');
      }
      this.funcaoPesquisa = this.procurarNaListaDeObjeto;
      // Inicia a lista sem filtros
      this.funcaoPesquisa('');
    }
    // Foca na input
    this.input.setFocus();
  }
}
