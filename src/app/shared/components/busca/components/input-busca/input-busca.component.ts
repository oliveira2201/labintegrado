import { Component, OnInit, Output, EventEmitter, Input, ViewChild } from '@angular/core';
import { ListaPopoverComponent } from '../lista-popover/lista-popover.component';
import { ModalController } from '@ionic/angular';
import { FormGroup, FormControl } from '@angular/forms';

/**
 * Realiza buscas.
 * Apresenta um modal com uma caixa de busca e uma lista com os resultados.
 * Pesquisa por índices no algolia, array com string e array com objeto.
 * Retorna uma array com o item escolhido.
 * Obrigatório: listaString: string[] || indice: string || listaObjeto: objeto[].
 * Caso objeto: Obrigatório: propriedadeBuscada: string
 * Opcional: obrigatorio: boolean.
 */
@Component({
  selector: 'app-input-busca',
  templateUrl: './input-busca.component.html',
  styleUrls: ['./input-busca.component.scss']
})
export class InputBuscaComponent implements OnInit {
  /**
   * Referencia do modal
   */
  modalRef;
  /**
   * Referencia da inputbox
   */
  @ViewChild('box') box;
  /**
   * Array com as strings a serem pesquisadas
   */
  @Input() listaString: string[];
  /**
   * Array com os objetos a ser pesquisado
   */
  @Input() listaObjeto: object[];
  /**
   * Propriedade do objeto a ser pesquisada
   */
  @Input() propriedadeBuscada: string;
  /**
   * Nome do índice do algolia a ser pesquisado
   */
  @Input() indice: string;
  /**
   * Label a ser utilizada na caixa de texto
   */
  @Input() label: string;
  /**
   * Controle do formulário a ser utilizado
   */
  @Input() controleFormulario: FormControl;
  /**
   * Se obrigatório, não apresenta o botão de OK.
   * Sai somente quando clicar em alguma opção.
   */
  @Input() obrigatorio: boolean;
  /**
   * Emiti o item selecionado
   */
  @Output() itemSelecionado = new EventEmitter<any>();
  /**
   * Parametros a serem passados para o modal
   */
  parametros = {};

  // ====================================================== //
  // ===================== Construtor ===================== //
  // ====================================================== //
  constructor(private modalCtrl: ModalController) {}

  // ====================================================== //
  // ======================= Eventos ====================== //
  // ====================================================== //
  /**
   * Abre o modal com os parâmetros entrados
   * Emite o item escolhido ao sair
   */
  async modal() {
    this.modalRef = await this.modalCtrl.create({
      component: ListaPopoverComponent,
      animated: true,
      keyboardClose: false,
      componentProps: this.parametros
    });
    this.modalRef.onDidDismiss().then(data => {
      console.log(data);
      if (data.data) {
        if (this.listaString) {
          this.box.value = data.data;
        }
        if (this.listaObjeto || this.indice) {
          if (data.data[this.propriedadeBuscada]) {
            this.box.value = data.data[this.propriedadeBuscada];
          } else {
            this.box.value = data.data;
          }
        }
        this.itemSelecionado.emit(data.data);
      }
    });
    return await this.modalRef.present();
  }

  // ====================================================== //
  // ==================== Ciclo de vida =================== //
  // ====================================================== //
  /**
   * Inicializa o componente
   */
  ngOnInit() {
    if (this.listaString) {
      // @ts-ignore
      this.parametros['listaString'] = this.listaString;
    }
    if (this.listaObjeto) {
      // @ts-ignore
      this.parametros['listaObjeto'] = this.listaObjeto;
      this.parametros['propriedadeBuscada'] = this.propriedadeBuscada;
    }

    if (this.indice) {
      // @ts-ignore
      this.parametros['indice'] = this.indice;
      this.parametros['propriedadeBuscada'] = this.propriedadeBuscada;
    }
    if (this.obrigatorio) {
      this.parametros['obrigatorio'] = this.obrigatorio;
    }
  }
}
