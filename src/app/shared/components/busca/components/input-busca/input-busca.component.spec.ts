import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule, ModalController } from '@ionic/angular';

import { InputBuscaComponent } from './input-busca.component';
import { ReactiveFormsModule } from '@angular/forms';

describe('InputBuscaComponent', () => {
  let component: InputBuscaComponent;
  let fixture: ComponentFixture<InputBuscaComponent>;
  let modalCtrlSpy: jasmine.SpyObj<ModalController>;

  // Criar spy do serviço
  const modalSpy = jasmine.createSpyObj('Modal', ['present', 'onDidDismiss']);

  // Criar spies
  modalSpy.onDidDismiss.and.callFake(() => {
    return Promise.resolve({ data: 'abacaxi' });
  });
  modalCtrlSpy = jasmine.createSpyObj('ServiceInjected', ['create']);
  // @ts-ignore
  modalCtrlSpy.create.and.callFake(() => {
    return modalSpy;
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InputBuscaComponent],
      imports: [IonicModule.forRoot(), ReactiveFormsModule],
      providers: [{ provide: ModalController, useValue: modalCtrlSpy }]
    }).compileComponents();

    fixture = TestBed.createComponent(InputBuscaComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  }));

  it('deve ser criado', () => {
    expect(component).toBeTruthy();
  });

  describe('Ao receber uma array de objetos', () => {
    it('deve receber na input o valor do item escolhido', async () => {
      modalSpy.onDidDismiss.and.callFake(() => {
        return Promise.resolve({ data: { id: '1', nome: 'abacaxi' } });
      });
      component.listaString = null;
      component.listaObjeto = [
        { id: '1', nome: 'abacaxi' },
        { id: '2', nome: 'abacate' },
        { id: '3', nome: 'acerola' },
        { id: '4', nome: 'morango' }
      ];
      component.propriedadeBuscada = 'nome';
      await component.modal();
      fixture.detectChanges();
      expect(component.box.value).toEqual('abacaxi');
    });
    it('deve emitir um objeto com o valor do item escolhido', async () => {
      modalSpy.onDidDismiss.and.callFake(() => {
        return Promise.resolve({ data: { id: '1', nome: 'abacaxi' } });
      });
      component.listaString = null;
      component.listaObjeto = [
        { id: '1', nome: 'abacaxi' },
        { id: '2', nome: 'abacate' },
        { id: '3', nome: 'acerola' },
        { id: '4', nome: 'morango' }
      ];
      component.propriedadeBuscada = 'nome';

      spyOn(component.itemSelecionado, 'emit');

      await component.modal();
      fixture.detectChanges();
      expect(component.itemSelecionado.emit).toHaveBeenCalledWith({ id: '1', nome: 'abacaxi' });
    });
  });

  describe('Ao receber uma array de strings', () => {
    it('deve receber na input o valor do item escolhido', async () => {
      component.listaString = ['abacaxi', 'abacate', 'acerola', 'morango'];
      component.listaObjeto = null;
      await component.modal();
      fixture.detectChanges();
      expect(component.box.value).toEqual('abacaxi');
    });
    it('deve emitir um objeto com o valor do item escolhido', async () => {
      component.listaString = ['abacaxi', 'abacate', 'acerola', 'morango'];

      spyOn(component.itemSelecionado, 'emit');

      await component.modal();
      fixture.detectChanges();
      expect(component.itemSelecionado.emit).toHaveBeenCalledWith('abacaxi');
    });
  });
});
