import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListaPopoverComponent } from './components/lista-popover/lista-popover.component';
import { InputBuscaComponent } from './components/input-busca/input-busca.component';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [ListaPopoverComponent, InputBuscaComponent],
  imports: [CommonModule, IonicModule, FormsModule, ReactiveFormsModule],
  exports: [InputBuscaComponent]
})
export class InputBuscaModule {}
