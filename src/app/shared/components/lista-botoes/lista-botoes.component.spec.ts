import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListaBotoesComponent } from './lista-botoes.component';

describe('ListaBotoesComponent', () => {
  let component: ListaBotoesComponent;
  let fixture: ComponentFixture<ListaBotoesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListaBotoesComponent],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListaBotoesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('deve ser criado', () => {
    expect(component).toBeTruthy();
  });
});
