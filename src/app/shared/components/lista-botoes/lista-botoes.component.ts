import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-lista-botoes',
  templateUrl: './lista-botoes.component.html',
  styleUrls: ['./lista-botoes.component.scss']
})
export class ListaBotoesComponent implements OnInit {
  @Input() lista;
  @Input() grupo: FormGroup;
  @Input() botaoSelecionadoIndice;
  @Output() selecionarEmitter = new EventEmitter();

  emitirSelecionar(i) {
    this.botaoSelecionadoIndice = i;
    this.selecionarEmitter.emit(i);
  }

  constructor() {}

  ngOnInit() {}
}
