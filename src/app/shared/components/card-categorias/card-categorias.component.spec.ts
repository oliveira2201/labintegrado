import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CardCategoriasComponent } from './card-categorias.component';
import { SharedModule } from '../../shared.module';

describe('CardCategoriasComponent', () => {
  let component: CardCategoriasComponent;
  let fixture: ComponentFixture<CardCategoriasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [],
      imports: [IonicModule.forRoot(), SharedModule]
    }).compileComponents();

    fixture = TestBed.createComponent(CardCategoriasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('deve ser criado', () => {
    expect(component).toBeTruthy();
  });
});
