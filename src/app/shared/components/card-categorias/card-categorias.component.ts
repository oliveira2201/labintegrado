import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ListaBotoesComponent } from '../lista-botoes/lista-botoes.component';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-card-categorias',
  templateUrl: './card-categorias.component.html',
  styleUrls: ['./card-categorias.component.scss']
})
export class CardCategoriasComponent implements OnInit {
  @Input() titulo;
  @Input() lista;
  @Input() botaoSelecionadoIndice;
  @Input() botaoEditar = false;
  @Input() semBarra = false;
  @Input() grupo: FormGroup;
  @Output() botaoSelecionarEmitter = new EventEmitter();
  @Output() botaoAdicionarEmitter = new EventEmitter();
  @Output() botaoEditarEmitter = new EventEmitter();
  @Output() botaoRemoverEmitter = new EventEmitter();

  emitirSelecionar(e) {
    this.botaoSelecionarEmitter.emit(e);
  }
  emitirAdicionar() {
    this.botaoAdicionarEmitter.emit();
  }
  emitirEditar() {
    this.botaoEditarEmitter.emit();
  }
  emitirRemover() {
    this.botaoRemoverEmitter.emit();
  }

  constructor() {}

  ngOnInit() {}
}
