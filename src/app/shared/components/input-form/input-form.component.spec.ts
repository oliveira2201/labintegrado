import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InputFormComponent } from './input-form.component';
import { ReactiveFormsModule, FormBuilder, Validators } from '@angular/forms';
import { BrMaskerModule } from 'br-mask';

describe('InputFormComponent', () => {
  let component: InputFormComponent;
  let fixture: ComponentFixture<InputFormComponent>;
  // Cria uma instancia do FormBuilder
  const fb: FormBuilder = new FormBuilder();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InputFormComponent],
      imports: [IonicModule, ReactiveFormsModule, BrMaskerModule]
    }).compileComponents();

    fixture = TestBed.createComponent(InputFormComponent);
    component = fixture.componentInstance;

    // Inicializa as inputs
    component.controleFormulario = fb.control('');
    component.label = 'Nome';

    fixture.detectChanges();
  }));

  it('deve ser criado', () => {
    expect(component).toBeTruthy();
  });

  it('a label deve estar presente quando dada', () => {
    component.label = 'Nome';
    fixture.detectChanges();
    // Inicializa as inputs
    const html: HTMLElement = fixture.nativeElement;
    const label = html.querySelector('ion-label').innerText;
    expect(component.label === label).toBeTruthy();
  });

  describe('deve apresentar o erro do controle', () => {
    beforeEach(() => {
      // Inicializa as inputs
      component.controleFormulario = fb.control('', [Validators.required]);
    });
    it('quando touched', () => {
      component.controleFormulario.markAllAsTouched();
      fixture.detectChanges();
      const html: HTMLElement = fixture.nativeElement;
      const erro = html.querySelector('span').innerText;
      expect(erro.includes('Campo obrigatório')).toBeTruthy();
    });
    it('quando dirt', () => {
      component.controleFormulario.markAsDirty();
      fixture.detectChanges();
      const html: HTMLElement = fixture.nativeElement;
      const erro = html.querySelector('span').innerText;
      expect(erro.includes('Campo obrigatório')).toBeTruthy();
    });
  });

  it('não deve apresentar o erro quando pristine', () => {
    // Inicializa as inputs
    component.controleFormulario = fb.control('', [Validators.required]);
    fixture.detectChanges();
    const html: HTMLElement = fixture.nativeElement;
    const erro = html.querySelector('span');
    expect(erro).toBeNull();
  });

  describe('deve apresentar os seguintes erros', () => {
    it('Campo obrigatório', () => {
      const controle = component.controleFormulario;
      controle.setErrors({ required: true });
      const erros = component.obterErros();
      expect(erros.includes('Campo obrigatório')).toBeTruthy();
    });
    it('Email com formato inválido', () => {
      const controle = component.controleFormulario;
      controle.setErrors({ email: true });
      const erros = component.obterErros();
      expect(erros.includes('Email com formato inválido')).toBeTruthy();
    });
    it('Campo deve ter pelo menos ${x} caracteres', () => {
      const controle = component.controleFormulario;
      controle.setErrors({ minlength: { requiredLength: 6 } });
      const erros = component.obterErros();
      expect(erros.includes('Campo deve ter pelo menos 6 caracteres')).toBeTruthy();
    });
  });

  it('Se a label for Senha ou Confirmar Senha, a input é do tipo password', () => {
    // Senha
    component.label = 'Senha';
    component.ngOnInit();
    fixture.detectChanges();
    let html: HTMLElement = fixture.nativeElement;
    let tipo = html.querySelector('ion-input');
    expect(tipo.attributes.getNamedItem('ng-reflect-type').value === 'password').toBeTruthy();

    // Confirmar Senha
    component.label = 'Confirmar Senha';
    component.ngOnInit();
    fixture.detectChanges();
    html = fixture.nativeElement;
    tipo = html.querySelector('ion-input');
    expect(tipo.attributes.getNamedItem('ng-reflect-type').value === 'password').toBeTruthy();

    // Outra
    component.label = 'Outra';
    component.ngOnInit();
    fixture.detectChanges();
    html = fixture.nativeElement;
    tipo = html.querySelector('ion-input');
    expect(tipo.attributes.getNamedItem('ng-reflect-type').value === 'password').toBeFalsy();
  });

  it('Se a label for Telefone, a input é do tipo ter mascára de telefone', () => {
    // Senha
    component.label = 'Telefone';
    component.ngOnInit();
    fixture.detectChanges();
    let html: HTMLElement = fixture.nativeElement;
    let tipo = html.querySelector('ion-input');
    expect(tipo.attributes.getNamedItem('ng-reflect-brmasker').value).toBeTruthy();
    // Outra
    component.label = 'Outra';
    component.ngOnInit();
    fixture.detectChanges();
    html = fixture.nativeElement;
    tipo = html.querySelector('ion-input');
    expect(tipo.attributes.getNamedItem('ng-reflect-brmasker')).toBeFalsy();
  });
});
