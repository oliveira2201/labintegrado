import { Component, OnInit, Input, forwardRef } from '@angular/core';
import { FormControl } from '@angular/forms';

interface BrMaskModel {
  mask?: string;
  len?: number;
  money?: boolean;
  phone?: boolean;
  phoneNotDDD?: boolean;
  person?: boolean;
  percent?: boolean;
  type?: 'alfa' | 'num' | 'all';
  decimal?: number;
  decimalCaracter?: string;
  thousand?: string;
  userCaracters?: false;
  numberAndTousand?: false;
  moneyInitHasInt?: true;
}

/**
 * Caixa de texto com validação para ser utilizada em formulários.
 * Atribui máscaras e tipos de caixa baseados na label.
 * Obrigatório: controleFormulario.
 * Opcional: label.
 * @example
 * Senha e Confirmar senha: caixa do tipo password
 * @example
 * Telefone: caixa com máscara de telefone
 * @example
 * Erros com mensagem: Tamanho mínimo, formato de email e campo obrigatório
 * @export
 */
@Component({
  selector: 'app-input-form',
  templateUrl: './input-form.component.html',
  styleUrls: ['./input-form.component.scss']
})
export class InputFormComponent implements OnInit {
  /**
   * Armazena o tipo de controle a ser utilizado na caixa de texto
   */
  tipoControle: string;

  /**
   * Armazena a mascára a ser utilizada pelo brmask
   */
  mask: BrMaskModel;

  /**
   * Label a ser utilizada na caixa de texto
   */
  @Input() label: string;

  /**
   * Controle do formulário a ser utilizado
   */
  @Input() controleFormulario: FormControl;

  /**
   * Transforma os erros do campo em um texto personalizado
   */
  obterErros() {
    // Array que será armazenado todos erros do controle
    const listaDeErros = [];
    // Array com todos erros do controle
    const erros = this.controleFormulario.errors;
    if (erros) {
      if (erros.minlength) {
        listaDeErros.push(`Campo deve ter pelo menos ${erros.minlength.requiredLength} caracteres`);
      }
      if (erros.required) {
        listaDeErros.push('Campo obrigatório');
      }

      if (erros.email) {
        listaDeErros.push('Email com formato inválido');
      }
    }
    return listaDeErros;
  }

  /**
   * Inicializa o componente
   */
  ngOnInit(): void {
    // Erro caso não tenha controle
    if (!this.controleFormulario) {
      throw new Error('Propriedade "controle" é obrigatória');
    }
    // Atribui o controle tipo password
    if (this.label === 'Senha' || this.label === 'Confirmar Senha') {
      this.tipoControle = 'password';
    } else {
      this.tipoControle = 'text';
    }
    // Atribui o máscara de telefone
    if (this.label === 'Telefone') {
      this.mask = { phone: true };
    } else {
      this.mask = null;
    }
  }
}
