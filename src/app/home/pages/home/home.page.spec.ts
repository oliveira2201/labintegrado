import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomePage } from './home.page';
import { AuthService } from 'src/app/core/services/auth.service';
import { Observable } from 'rxjs';
import { RouterModule } from '@angular/router';

const authServiceStub = {
  isAdmin: () => {
    return true;
  },
  usuarioToken: () => {
    return {
      nome: 'Admin',
      uid: '123'
    };
  },
  authState$: {
    subscribe: () => {
      return new Observable(subscriber => {
        subscriber.next({
          nomeAnimal: 'animalNome'
        });
      });
    }
  }
};

describe('HomePage', () => {
  let component: HomePage;
  let fixture: ComponentFixture<HomePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HomePage],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [RouterModule.forRoot([])],
      providers: [{ provide: AuthService, useValue: authServiceStub }]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
