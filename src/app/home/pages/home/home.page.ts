import { Component } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-tabs',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage {
  constructor(public auth: AuthService, private navCtrl: NavController) {}
  logout() {
    this.auth.logout();
    window.location.reload();
  }
}
