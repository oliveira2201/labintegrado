import { Component, OnInit } from '@angular/core';
import { ExamesService } from 'src/app/exames/services/exames/exames.service';
import { first } from 'rxjs/operators';
import { FormBuilder, FormControl, FormArray } from '@angular/forms';
import { TabelaPrecosService } from '../services/tabela-precos.service';
import { NavController, AlertController } from '@ionic/angular';
import { OverlayService } from 'src/app/shared/services/overlay/overlay.service';

@Component({
  selector: 'app-tabela-precos-lista',
  templateUrl: './tabela-precos-lista.page.html',
  styleUrls: ['./tabela-precos-lista.page.scss']
})
export class TabelaPrecosListaPage {
  tabelasPrecos;
  tabelaPrecosSelecionada;
  tabelaPrecosForm = this.fb.group({
    id: '',
    nome: '',
    itens: this.fb.array([])
  });
  constructor(
    private examesService: ExamesService,
    private fb: FormBuilder,
    private tabelaPrecosService: TabelaPrecosService,
    private navCtrl: NavController,
    private overlayService: OverlayService,
    private alertCrtl: AlertController
  ) {}

  async criarTabela(nome) {
    const tabelaNova = {
      nome: nome,
      itens: []
    };
    this.examesService.obterTodos().subscribe(async exames => {
      for (const exame of exames) {
        tabelaNova.itens.push({
          id: exame.id,
          nome: exame.nome,
          valor: exame.valor
        });
      }
      this.carregarTabelaParaEdicao(tabelaNova);
      this.salvar();
    });
  }

  removerTabela() {
    this.tabelaPrecosService.delete(this.tabelaPrecosForm.value);
  }

  criarSlug(str) {
    return str
      .toLowerCase()
      .replace(/[àÀáÁâÂãäÄÅåª]+/g, 'a') // Special Characters #1
      .replace(/[èÈéÉêÊëË]+/g, 'e') // Special Characters #2
      .replace(/[ìÌíÍîÎïÏ]+/g, 'i') // Special Characters #3
      .replace(/[òÒóÓôÔõÕöÖº]+/g, 'o') // Special Characters #4
      .replace(/[ùÙúÚûÛüÜ]+/g, 'u') // Special Characters #5
      .replace(/[ýÝÿŸ]+/g, 'y') // Special Characters #6
      .replace(/[ñÑ]+/g, 'n') // Special Characters #7
      .replace(/[çÇ]+/g, 'c') // Special Characters #8
      .replace(/[ß]+/g, 'ss') // Special Characters #9
      .replace(/[Ææ]+/g, 'ae') // Special Characters #10
      .replace(/[Øøœ]+/g, 'oe') // Special Characters #11
      .replace(/[%]+/g, 'pct') // Special Characters #12
      .replace(/\s+/g, '-') // Replace spaces with -
      .replace(/[^\w\-]+/g, '') // Remove all non-word chars
      .replace(/\-\-+/g, '-') // Replace multiple - with single -
      .replace(/^-+/, '') // Trim - from start of text
      .replace(/-+$/, ''); // Trim - from end of text
  }

  carregarTabelaParaEdicao(dados) {
    const formBranco = {
      id: '',
      nome: '',
      itens: []
    };

    const tabelaNova = {
      id: dados.id || '',
      nome: dados.nome,
      itens: dados.itens
    };
    const itensCtrl = this.tabelaPrecosForm.controls.itens as FormArray;
    itensCtrl.clear();
    this.tabelaPrecosForm.setValue(formBranco);

    for (const item of dados.itens) {
      const itens = this.tabelaPrecosForm.get('itens') as FormArray;
      itens.push(this.retornarExame());
    }

    this.tabelaPrecosForm.patchValue(tabelaNova);
  }

  retornarExame() {
    return this.fb.group({
      id: '',
      nome: '',
      valor: ''
    });
  }

  async salvar() {
    const loading = await this.overlayService.loading({
      message: 'Salvando...'
    });
    try {
      this.tabelaPrecosForm.value.id
        ? this.tabelaPrecosService.atualizar(this.tabelaPrecosForm.value)
        : this.tabelaPrecosService.criar(
            this.tabelaPrecosForm.value,
            this.criarSlug(this.tabelaPrecosForm.value.nome)
          );
      // this.navCtrl.back();
    } catch (error) {
      console.log(error);
    } finally {
      loading.dismiss();
    }
  }

  voltar() {
    console.log(this.tabelaPrecosForm.value);
  }

  async apresentarAlerta() {
    const alert = await this.alertCrtl.create({
      message: 'Digite o nome da lista a ser criada',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel'
        },
        {
          text: 'OK',
          handler: data => {
            this.criarTabela(data.nome);
          }
        }
      ],
      inputs: [
        {
          name: 'nome',
          type: 'text'
        }
      ]
    });

    await alert.present();
  }

  selecionarLista(ev) {
    this.carregarTabelaParaEdicao(this.tabelasPrecos[ev]);
    // this.tabelaPrecosForm.patchValue(this.tabelasPrecos[ev]);
  }

  async ionViewDidEnter() {
    this.tabelaPrecosService
      .obterTodos()
      .subscribe(tabelasPrecos => (this.tabelasPrecos = tabelasPrecos));
  }
}
