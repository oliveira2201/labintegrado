import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TabelaPrecosListaPage } from './tabela-precos-lista.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { TabelaPrecosService } from '../services/tabela-precos.service';
import { Observable } from 'rxjs';
import { ExamesService } from 'src/app/exames/services/exames/exames.service';
import { RouterModule } from '@angular/router';

const examesServiceStub = {
  obterPorId: () => {
    return new Observable(subscriber => {
      subscriber.next({
        nomeAnimal: 'animalNome'
      });
    });
  }
};

const tabelaPrecosServiceStub = {
  obterPorId: () => {
    return new Observable(subscriber => {
      subscriber.next({
        nomeAnimal: 'animalNome'
      });
    });
  }
};

describe('TabelaPrecosListaPage', () => {
  let component: TabelaPrecosListaPage;
  let fixture: ComponentFixture<TabelaPrecosListaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TabelaPrecosListaPage],
      imports: [IonicModule.forRoot(), SharedModule, RouterModule.forRoot([])],
      providers: [
        { provide: TabelaPrecosService, useValue: tabelaPrecosServiceStub },
        { provide: ExamesService, useValue: examesServiceStub }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(TabelaPrecosListaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('deve ser criado', () => {
    expect(component).toBeTruthy();
  });
});
