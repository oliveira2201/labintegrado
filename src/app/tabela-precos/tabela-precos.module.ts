import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TabelaPrecosRoutingModule } from './tabela-precos-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    TabelaPrecosRoutingModule
  ]
})
export class TabelaPrecosModule { }
