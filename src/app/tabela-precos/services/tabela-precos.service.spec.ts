import { TestBed } from '@angular/core/testing';

import { TabelaPrecosService } from './tabela-precos.service';
import { AngularFirestore } from '@angular/fire/firestore';

const angularFirestoreStub = {
  collection: () => {
    return;
  }
};

describe('TabelaPrecosService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [{ provide: AngularFirestore, useValue: angularFirestoreStub }]
    })
  );

  it('deve ser criado', () => {
    const service: TabelaPrecosService = TestBed.get(TabelaPrecosService);
    expect(service).toBeTruthy();
  });
});
