import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Repository } from 'src/app/core/classes/repository.class';
import { TabelaPrecos } from '../models/listas.model';
import { ListasService } from 'src/app/listas/services/listas.service';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TabelaPrecosService extends Repository<TabelaPrecos> {
  batch
  constructor(db: AngularFirestore, private listaService: ListasService) {
    super(db);
    this.init();
  }

  private init(): void {
    this.setCollection('/tabelaPrecos');
  }

  // @ts-ignore
  async criar(item: TabelaPrecos, id: string): Promise<TabelaPrecos> {
    this.batch = this.db.firestore.batch();
    const tabelaRef = this.db.collection('tabelaPrecos').doc(id).ref;
    item.id = id;
    this.batch.set(tabelaRef, item);
    this.atualizarLista(item, id);
  }

  async atualizarLista(item, id = '') {
    const exameListaRef = this.db.collection('listas').doc('TabelasPrecos').ref;
    const itemNovo = {
      id: item.id || id,
      nome: item.nome,
      valor: item.valor || ''
    };

    await this.listaService
      .obterPorId('TabelasPrecos')
      .pipe(take(1))
      .subscribe(data => {
        // Se não tiver a lista, criar uma em branco
        if (!data) {
          const listaVazia = {
            id: 'TabelasPrecos',
            nome: 'TabelasPrecos',
            itens: []
          };
          // @ts-ignore
          this.listaService.criar(listaVazia, 'TabelasPrecos');
          // @ts-ignore
          data = listaVazia;
          // @ts-ignore
          data.itens.push(itemNovo);
          this.batch.update(exameListaRef, data);
          this.batch.commit();
        } else {
          // Verifica se exame já existe na lista
          let itemEncontrado = this.procurarNoObjeto(data, 'nome', item.nome);
          if (itemEncontrado) {
            // Garantir que esteja atualizado caso sobrescrito
            Object.assign(itemEncontrado, itemNovo);
            this.batch.update(exameListaRef, data);
            this.batch.commit();
            return;
          } else {
            // @ts-ignore
            data.itens.push(itemNovo);
            this.batch.update(exameListaRef, data);
            this.batch.commit();
          }
        }
      });
  }

  async delete(item: TabelaPrecos): Promise<void> {
    this.batch = this.db.firestore.batch();
    const tabelaRef = this.db.collection('tabelaPrecos').doc(item.id).ref;
    this.batch.delete(tabelaRef);

    const exameListaRef = this.db.collection('listas').doc('TabelasPrecos').ref;
    await this.listaService
      .obterPorId('TabelasPrecos')
      .pipe(take(1))
      .subscribe((data: any) => {
        if (data) {
          for (const [i, element] of data.itens.entries()) {
            if (element.nome === item.nome) {
              data.itens.splice(i, 1);
              this.batch.update(exameListaRef, data);
              this.batch.commit();
              return;
            }
          }
          this.batch.commit();
        } else {
          this.batch.commit();
        }
      });
  }

  procurarNoObjeto(object, prop, value) {
    if (object.hasOwnProperty(prop) && object[prop] == value) return object;

    for (var i = 0; i < Object.keys(object).length; i++) {
      if (typeof object[Object.keys(object)[i]] == 'object') {
        var o = this.procurarNoObjeto(object[Object.keys(object)[i]], prop, value);
        if (o != null) return o;
      }
    }
  }
}
