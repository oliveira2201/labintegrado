import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import {
  AngularFireAuthGuard,
  hasCustomClaim,
  redirectUnauthorizedTo,
  redirectLoggedInTo
} from '@angular/fire/auth-guard';
const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['login']);
const adminOnly = () => hasCustomClaim('admin');

const routes: Routes = [
  {
    path: 'home',
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToLogin },

    loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
  },
  {
    path: 'configuracoes',
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: adminOnly },

    loadChildren: () =>
      import('./configuracoes/configuracoes.module').then(m => m.ConfiguracoesModule)
  },
  {
    path: 'requisicoes',
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToLogin },
    loadChildren: () =>
      import('./requisicoes/requisicoes-routing.module').then(m => m.RequisicoesRoutingModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./core/pages/login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: 'precos',
    loadChildren: () =>
      import('./tabela-precos/tabela-precos-routing.module').then(m => m.TabelaPrecosRoutingModule)
  },
  {
    path: '**',
    redirectTo: 'home'
  }
];
@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
