import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExamesRoutingModule } from './exames-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ExamesRoutingModule
  ]
})
export class ExamesModule { }
