import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ApplicationRef
} from '@angular/core';

@Component({
  selector: 'app-exames-form-campos',
  templateUrl: './exames-form-campos.component.html',
  styleUrls: ['./exames-form-campos.component.scss']
})
export class ExamesFormCamposComponent implements OnInit {
  @Input() campos;
  @Input() especies;
  @Input() campoSelecionado;
  @Input() campoSelecionadoIndice;
  @Input() especieSelecionada;
  @Input() especieSelecionadaIndice;
  @Input() listaEspecies;
  @Input() referencias;
  @Input() listaTipoDeDado;
  @Output() campoSelecionadoIndiceEmmiter = new EventEmitter();
  @Output() adicionarCampoEmmiter = new EventEmitter();
  @Output() removerCampoEmmiter = new EventEmitter();
  @Output() especieSelecionadaIndiceEmmiter = new EventEmitter();
  @Output() adicionarEspecieEmmiter = new EventEmitter();
  @Output() removerEspecieEmmiter = new EventEmitter();
  @Output() adicionarReferenciaEmmiter = new EventEmitter();
  @Output() removerReferenciaEmmiter = new EventEmitter();
  constructor() {}

  campoSelecionadoEmitter(e) {
    this.campoSelecionadoIndiceEmmiter.emit(e);
  }
  adicionarCampo() {
    this.adicionarCampoEmmiter.emit();
  }
  removerCampo() {
    this.removerCampoEmmiter.emit();
  }

  especieSelecionadaEmitter(e) {
    this.especieSelecionadaIndiceEmmiter.emit(e);
  }
  adicionarEspecie() {
    this.adicionarEspecieEmmiter.emit();
  }
  removerEspecie() {
    this.removerEspecieEmmiter.emit();
  }

  adicionarReferencia() {
    this.adicionarReferenciaEmmiter.emit();
  }
  removerReferencia(index) {
    this.removerReferenciaEmmiter.emit(index);
  }

  ngOnInit() {}
}
