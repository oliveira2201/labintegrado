import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ExamesFormCamposComponent } from './exames-form-campos.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ExamesFormTabelaComponent } from '../exames-form-tabela/exames-form-tabela.component';

describe('ExamesFormCamposComponent', () => {
  let component: ExamesFormCamposComponent;
  let fixture: ComponentFixture<ExamesFormCamposComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ExamesFormCamposComponent, ExamesFormTabelaComponent],
      imports: [IonicModule.forRoot(), SharedModule]
    }).compileComponents();

    fixture = TestBed.createComponent(ExamesFormCamposComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('deve ser criado', () => {
    expect(component).toBeTruthy();
  });
});
