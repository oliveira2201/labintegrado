import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ExamesFormTabelaComponent } from './exames-form-tabela.component';
import { SharedModule } from 'src/app/shared/shared.module';

describe('ExamesFormTabelaComponent', () => {
  let component: ExamesFormTabelaComponent;
  let fixture: ComponentFixture<ExamesFormTabelaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ExamesFormTabelaComponent],
      imports: [IonicModule.forRoot(), SharedModule]
    }).compileComponents();

    fixture = TestBed.createComponent(ExamesFormTabelaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('deve ser criado', () => {
    expect(component).toBeTruthy();
  });
});
