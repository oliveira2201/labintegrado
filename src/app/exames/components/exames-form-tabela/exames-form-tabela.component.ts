import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-exames-form-tabela',
  templateUrl: './exames-form-tabela.component.html',
  styleUrls: ['./exames-form-tabela.component.scss']
})
export class ExamesFormTabelaComponent implements OnInit {
  @Input() especies;
  @Input() especieSelecionada;
  @Input() especieSelecionadaIndice;
  @Input() listaEspecies;
  @Input() referencias;
  @Output() especieSelecionadaIndiceEmmiter = new EventEmitter();
  @Output() adicionarEspecieEmitter = new EventEmitter();
  @Output() removerEspecieEmitter = new EventEmitter();
  @Output() adicionarReferenciaEmitter = new EventEmitter();
  @Output() removerReferenciaEmitter = new EventEmitter();

  especieSelecionadaEmitter(e) {
    this.especieSelecionadaIndiceEmmiter.emit(e);
  }

  adicionarEspecie() {
    this.adicionarEspecieEmitter.emit();
  }
  removerEspecie() {
    this.removerEspecieEmitter.emit();
  }

  adicionarReferencia() {
    this.adicionarReferenciaEmitter.emit();
  }
  removerReferencia(index) {
    this.removerReferenciaEmitter.emit(index);
  }

  constructor() {}

  ngOnInit() {}
}
