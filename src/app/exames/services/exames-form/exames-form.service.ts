import { Injectable, ApplicationRef } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';

@Injectable()
export class ExamesFormService {
  public form: FormGroup;
  private _campoSelecionadoIndice;
  private _especieSelecionadaIndice;
  private _referenciaSelecionadaIndice;

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      nome: '',
      categoria: '',
      valor: '',
      campos: this.fb.array([])
    });
  }

  // *******************Campos********************* //
  get campos() {
    return this.form.get('campos') as FormArray;
  }

  get campoSelecionado() {
    return this.campos.at(this._campoSelecionadoIndice);
  }

  get campoSelecionadoIndice() {
    return this._campoSelecionadoIndice;
  }

  selecionarCampo(indice) {
    this._campoSelecionadoIndice = indice;
  }

  criarCampo(): FormGroup {
    return this.fb.group({
      nome: '',
      unidade: '',
      ordem: '',
      subcategoria: '',
      valorPadrao: '',
      tipoDeDado: '',
      especies: this.fb.array([])
    });
  }

  // Selecionar o campo recem adicionado, sem bugar a inicialização
  adicionarCampo() {
    const exameCampo = this.criarCampo();
    this.campos.push(exameCampo);
    return exameCampo;
  }
  retornarCampo() {
    const exameCampo = this.criarCampo();
    return exameCampo;
  }

  removerCampo() {
    this.campos.removeAt(this.campoSelecionadoIndice);
  }

  // *******************Espécies****************** //
  get especies() {
    if (!this.campoSelecionado) return;
    const a = this.campoSelecionado.get('especies') as FormArray;
    return a;
  }

  get especieSelecionada() {
    if (!this.especies) return;
    return this.especies.at(this.especieSelecionadaIndice);
  }

  get especieSelecionadaIndice() {
    return this._especieSelecionadaIndice;
  }

  selecionarEspecie(indice) {
    this._especieSelecionadaIndice = indice;
  }

  criarEspecie(): FormGroup {
    return this.fb.group({
      nome: '',
      referencias: this.fb.array([])
    });
  }

  adicionarEspecie() {
    const exameEspecie = this.criarEspecie();
    this.especies.push(exameEspecie);
    return exameEspecie;
  }
  retornarEspecie() {
    const exameEspecie = this.criarEspecie();
    return exameEspecie;
  }

  removerEspecie() {
    this.especies.removeAt(this.especieSelecionadaIndice);
  }

  // *******************Referencia****************** //
  get referencias() {
    if (!this.especieSelecionada) return;
    const a = this.especieSelecionada.get('referencias') as FormArray;
    return a;
  }

  get referenciaSelecionada() {
    return this.referencias.at(this.referenciaSelecionadaIndice);
  }

  get referenciaSelecionadaIndice() {
    return this._referenciaSelecionadaIndice;
  }

  criarReferencia(): FormGroup {
    return this.fb.group({
      idadeMinima: '',
      idadeMaxima: '',
      valorMinimo: '',
      valorMaximo: ''
    });
  }

  adicionarReferencia() {
    const exameReferencia = this.criarReferencia();
    // const nome = this.especies.value[this.especieSelecionadaIndice].nome;
    this.referencias.push(exameReferencia);
    // this.especies.value[this.especieSelecionadaIndice].nome = nome;
    return exameReferencia;
  }

  retornarReferencia() {
    const exameReferencia = this.criarReferencia();
    return exameReferencia;
  }

  removerReferencia(index) {
    this.referencias.removeAt(index);
  }
}
