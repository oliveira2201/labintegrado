import { TestBed } from '@angular/core/testing';

import { ExamesFormService } from './exames-form.service';
import { ReactiveFormsModule } from '@angular/forms';

describe('ExamesFormService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      providers: [ExamesFormService]
    })
  );

  it('should be created', () => {
    const service: ExamesFormService = TestBed.get(ExamesFormService);
    expect(service).toBeTruthy();
  });
});
