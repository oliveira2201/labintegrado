import { TestBed } from '@angular/core/testing';

import { ExamesLoaderService } from './exames-loader.service';
import { ExamesFormService } from '../exames-form/exames-form.service';
import { Observable } from 'rxjs';

const examesFormServiceStub = {
  obterPorId: () => {
    return new Observable(subscriber => {
      subscriber.next({
        nomeAnimal: 'animalNome'
      });
    });
  }
};

describe('LoaderService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [
        ExamesLoaderService,
        { provide: ExamesFormService, useValue: examesFormServiceStub }
      ]
    })
  );

  it('should be created', () => {
    const service: ExamesLoaderService = TestBed.get(ExamesLoaderService);
    expect(service).toBeTruthy();
  });
});
