import { Injectable } from '@angular/core';
import { ExamesFormService } from '../exames-form/exames-form.service';
import { Exame } from '../../models/exames.model';
import { FormArray } from '@angular/forms';

@Injectable()
export class ExamesLoaderService {
  constructor(private examesFormService: ExamesFormService) {}

  carregarExameParaEdicao(dados: Exame) {
    const form = this.examesFormService.form;
    const formCampos = this.examesFormService.form.controls.campos as FormArray;
    dados.campos.forEach((campo, indiceCampo) => {
      const campoNovo = this.examesFormService.retornarCampo();
      formCampos.push(campoNovo);
      const formEspecies = formCampos.controls[indiceCampo]['controls']
        .especies as FormArray;
      campo.especies.forEach((especie, indiceEspecie) => {
        const especieNova = this.examesFormService.retornarEspecie();
        formEspecies.push(especieNova);
        const formReferencias = formEspecies.controls[indiceEspecie]['controls']
          .referencias as FormArray;
        especie.referencias.forEach(element => {
          const referenciaNova = this.examesFormService.retornarReferencia();
          formReferencias.push(referenciaNova);
        });
      });
    });
    form.patchValue({ ...dados });
  }
}
