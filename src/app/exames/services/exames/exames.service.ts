import { Injectable } from '@angular/core';
import { Exame } from '../../models/exames.model';
import { Repository } from 'src/app/core/classes/repository.class';
import { AngularFirestore } from '@angular/fire/firestore';
import { ListasService } from 'src/app/listas/services/listas.service';
import { first, pairwise, take } from 'rxjs/operators';
import { Lista } from 'src/app/listas/models/listas.model';
import { TabelaPrecosService } from 'src/app/tabela-precos/services/tabela-precos.service';

@Injectable({
  providedIn: 'root'
})
export class ExamesService extends Repository<Exame> {
  batch;

  constructor(db: AngularFirestore, private tabelaPrecosService: TabelaPrecosService) {
    super(db);
    this.init();
  }

  private init(): void {
    this.setCollection('/exames');
  }

  // @ts-ignore
  async criar(item: Exame, id: string): Promise<Exame> {
    this.batch = this.db.firestore.batch();
    const exameRef = this.db.collection('exames').doc(id).ref;
    item.id = id;
    this.batch.set(exameRef, item);
    this.atualizarLista(item, id);
  }
  // @ts-ignore
  async atualizar(item) {
    this.batch = this.db.firestore.batch();
    const exameRef = this.db.collection('exames').doc(item.id).ref;
    this.batch.update(exameRef, item);
    this.atualizarLista(item);
  }

  async atualizarLista(item, id = '') {
    const tabelaRef = this.db.collection('tabelaPrecos');

    const itemNovo = {
      id: item.id,
      nome: item.nome,
      valor: item.valor
    };
    this.tabelaPrecosService
      .obterTodos()
      .pipe(take(1))
      .subscribe(tabelas => {
        for (const tabela of tabelas) {
          // Verifica se exame já existe na lista
          let itemEncontrado = this.procurarNoObjeto(tabela, 'nome', item.nome);

          if (!itemEncontrado) {
            // @ts-ignore
            tabela.itens.push(itemNovo);
            this.batch.update(tabelaRef.doc(tabela.id).ref, tabela);
          }
        }
        this.batch.commit();
      });
  }

  async delete(item: Exame): Promise<void> {
    this.batch = this.db.firestore.batch();
    const exameRef = this.db.collection('exames').doc(item.id).ref;
    this.batch.delete(exameRef);

    const tabelaRef = this.db.collection('tabelaPrecos');

    this.tabelaPrecosService
      .obterTodos()
      .pipe(take(1))
      .subscribe(tabelas => {
        for (const tabela of tabelas) {
          // Verifica se exame já existe na lista
          // @ts-ignore
          for (const [i, exame] of tabela.itens.entries()) {
            // @ts-ignore
            if (exame.nome === item.nome) {
              // @ts-ignore
              tabela.itens.splice(i, 1);
              this.batch.update(tabelaRef.doc(tabela.id).ref, tabela);
            }
          }
        }
        this.batch.commit();
      });
  }

  procurarNoObjeto(object, prop, value) {
    if (object.hasOwnProperty(prop) && object[prop] == value) return object;

    for (var i = 0; i < Object.keys(object).length; i++) {
      if (typeof object[Object.keys(object)[i]] == 'object') {
        var o = this.procurarNoObjeto(object[Object.keys(object)[i]], prop, value);
        if (o != null) return o;
      }
    }
  }
}
