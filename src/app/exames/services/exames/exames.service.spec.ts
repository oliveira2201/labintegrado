import { TestBed } from '@angular/core/testing';

import { ExamesService } from './exames.service';
import { AngularFirestore } from '@angular/fire/firestore';

const angularFirestoreStub = {
  collection: () => {
    return;
  }
};

describe('ExamesService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [{ provide: AngularFirestore, useValue: angularFirestoreStub }]
    })
  );

  it('deve ser criado', () => {
    const service: ExamesService = TestBed.get(ExamesService);
    expect(service).toBeTruthy();
  });
});
