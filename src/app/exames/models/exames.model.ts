export interface Exame {
  id: string;
  nome: string;
  categoria: string;
  valor: string;
  campos: Campos[];
}

export interface Campos {
  nome: string;
  unidade: string;
  ordem: number;
  valorPadrao: string;
  tipoDeDado: string;
  subcategoria: string;
  especies: [
    {
      nome: string;
      referencias: [
        {
          idadeMinima: string;
          idadeMaxima: string;
          valorMinimo: string;
          valorMaximo: string;
        }
      ];
    }
  ];
}
