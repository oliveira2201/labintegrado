import { Component, OnInit } from '@angular/core';
import { ExamesService } from '../../services/exames/exames.service';
import { OverlayService } from 'src/app/shared/services/overlay/overlay.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-exames-lista',
  templateUrl: './exames-lista.page.html',
  styleUrls: ['./exames-lista.page.scss']
})
export class ExamesListaPage implements OnInit {
  dados;
  exameSelecionadoId;
  examesServiceSub;
  get exameSelecionado() {
    if (this.exameSelecionadoId >= 0) {
      return this.dados[this.exameSelecionadoId];
    }
    return null;
  }
  examesCarregados = false;

  constructor(
    private examesService: ExamesService,
    private overlayService: OverlayService,
    public navCtrl: NavController
  ) {}

  adicionarExame() {
    this.navCtrl.navigateForward(`/configuracoes/exames/criar`);
  }

  editarExame() {
    if (this.exameSelecionado) {
      this.navCtrl.navigateForward(`/configuracoes/exames/editar/${this.exameSelecionado.id}`);
    }
  }

  removerExame() {
    if (this.exameSelecionado) {
      this.examesService.delete(this.exameSelecionado);
      this.dados.splice(this.exameSelecionadoId, 1);
    }
  }

  selecionarExame(index) {
    this.exameSelecionadoId = index;
  }

  async ngOnInit() {
    const loading = await this.overlayService.loading({
      message: 'Carregando...'
    });
    try {
      this.examesServiceSub = this.examesService.obterTodos().subscribe(dados => {
        this.dados = dados;
      });
    } catch (error) {
      console.log(error);
    } finally {
      loading.dismiss();
      this.examesCarregados = true;
    }
  }

  ngOnDestroy() {
    this.examesServiceSub.unsubscribe();
  }
}
