import { NgModule } from '@angular/core';
import { ExamesListaPage } from './exames-lista.page';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: ExamesListaPage
  }
];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [ExamesListaPage]
})
export class ExamesListaPageModule {}
