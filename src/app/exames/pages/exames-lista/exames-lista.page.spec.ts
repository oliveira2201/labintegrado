import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ExamesListaPage } from './exames-lista.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { Observable } from 'rxjs';
import { ExamesService } from '../../services/exames/exames.service';
import { RouterModule } from '@angular/router';

const examesServiceStub = {
  obterPorId: () => {
    return new Observable(subscriber => {
      subscriber.next({
        nomeAnimal: 'animalNome'
      });
    });
  }
};

describe('ExamesListaPage', () => {
  let component: ExamesListaPage;
  let fixture: ComponentFixture<ExamesListaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ExamesListaPage],
      imports: [IonicModule.forRoot(), SharedModule, RouterModule.forRoot([])],
      providers: [{ provide: ExamesService, useValue: examesServiceStub }]
    }).compileComponents();

    fixture = TestBed.createComponent(ExamesListaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
