import { Component, OnInit, ApplicationRef } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ExamesService } from '../../services/exames/exames.service';
import { first, pairwise } from 'rxjs/operators';
import { OverlayService } from 'src/app/shared/services/overlay/overlay.service';
import { ExamesFormService } from '../../services/exames-form/exames-form.service';
import { ExamesLoaderService } from '../../services/exames-loader/exames-loader.service';
import { NavController, PopoverController } from '@ionic/angular';
import { ListasService } from 'src/app/listas/services/listas.service';

@Component({
  selector: 'app-exames-lista',
  templateUrl: './exames-modificar.page.html',
  styleUrls: ['./exames-modificar.page.scss'],
  providers: [ExamesFormService, ExamesLoaderService]
})
export class ExamesModificarPage {
  // *********************Variaveis**********************//
  modoEdicao = false;
  exameId;
  examesCarregado = false;
  erro: string;
  listaCategorias;
  listaEspecies;
  listaTipoDeDado;
  // **********************Constructor*******************//
  constructor(
    private route: ActivatedRoute,
    public navCtrl: NavController,
    public popoverCtrl: PopoverController,
    private examesService: ExamesService,
    private overlayService: OverlayService,
    public examesFormService: ExamesFormService,
    private examesLoaderService: ExamesLoaderService,
    private listasService: ListasService
  ) {}
  // **********************Helpers***********************//
  criarSlug(str) {
    return str
      .toLowerCase()
      .replace(/[àÀáÁâÂãäÄÅåª]+/g, 'a') // Special Characters #1
      .replace(/[èÈéÉêÊëË]+/g, 'e') // Special Characters #2
      .replace(/[ìÌíÍîÎïÏ]+/g, 'i') // Special Characters #3
      .replace(/[òÒóÓôÔõÕöÖº]+/g, 'o') // Special Characters #4
      .replace(/[ùÙúÚûÛüÜ]+/g, 'u') // Special Characters #5
      .replace(/[ýÝÿŸ]+/g, 'y') // Special Characters #6
      .replace(/[ñÑ]+/g, 'n') // Special Characters #7
      .replace(/[çÇ]+/g, 'c') // Special Characters #8
      .replace(/[ß]+/g, 'ss') // Special Characters #9
      .replace(/[Ææ]+/g, 'ae') // Special Characters #10
      .replace(/[Øøœ]+/g, 'oe') // Special Characters #11
      .replace(/[%]+/g, 'pct') // Special Characters #12
      .replace(/\s+/g, '-') // Replace spaces with -
      .replace(/[^\w\-]+/g, '') // Remove all non-word chars
      .replace(/\-\-+/g, '-') // Replace multiple - with single -
      .replace(/^-+/, '') // Trim - from start of text
      .replace(/-+$/, ''); // Trim - from end of text
  }
  voltar() {
    this.navCtrl.navigateBack('/configuracoes/exames');
  }
  classificarCamposPorOrdem() {
    // Ordena conforme o campo ordem
    this.form.value.campos = this.form.value.campos.sort(function(a, b) {
      var x = parseInt(a.ordem, 10);
      var y = parseInt(b.ordem, 10);
      return x < y ? -1 : x > y ? 1 : 0;
    });
  }
  // ************************Form************************//
  get form(): FormGroup {
    return this.examesFormService.form;
  }
  async salvar() {
    const loading = await this.overlayService.loading({
      message: 'Salvando...'
    });
    try {
      this.classificarCamposPorOrdem();
      const exame = !this.modoEdicao
        ? await this.examesService.criar(this.form.value, this.criarSlug(this.form.value.nome))
        : await this.examesService.atualizar({
            id: this.exameId,
            ...this.form.value
          });
    } catch (e) {
      console.error(e);
      this.erro = e;
    } finally {
      loading.dismiss();
      this.navCtrl.navigateBack('/configuracoes/exames');
    }
  }
  // ***********************Campos***********************//
  selecionarCampo(e) {
    this.examesFormService.selecionarCampo(e);
  }
  adicionarCampoForm() {
    this.examesFormService.adicionarCampo();
    const totalCampos = this.examesFormService.campos.length;
    this.selecionarCampo(totalCampos - 1);
  }
  removerCampoForm() {
    this.examesFormService.removerCampo();
    const campoAnterior =
      this.examesFormService.campoSelecionadoIndice === 0
        ? 0
        : this.examesFormService.campoSelecionadoIndice - 1;
    this.selecionarCampo(campoAnterior);
  }
  // **********************Especie***********************//
  selecionarEspecie(e) {
    this.examesFormService.selecionarEspecie(e);
  }

  async adicionarEspecieForm(ev) {
    this.examesFormService.adicionarEspecie();
    const totalCampos = this.examesFormService.especies.length;
    this.selecionarEspecie(totalCampos - 1);
  }
  removerEspecieForm() {
    this.examesFormService.removerEspecie();
    const especieAnterior =
      this.examesFormService.especieSelecionadaIndice === 0
        ? 0
        : this.examesFormService.especieSelecionadaIndice - 1;
    this.selecionarEspecie(especieAnterior);
  }
  // *********************Referencias********************//
  adicionarReferenciaForm() {
    this.examesFormService.adicionarReferencia();
  }
  removerReferenciaForm(index) {
    this.examesFormService.removerReferencia(index);
  }
  // ***********************Init*************************//
  async ionViewDidEnter() {
    const loading = await this.overlayService.loading({
      message: 'Carregando...'
    });
    try {
      this.exameId = this.route.snapshot.paramMap.get('id');
      this.modoEdicao = !!this.exameId;

      this.listaCategorias = await this.listasService
        .obterPorId('Categorias')
        .pipe(first())
        .toPromise();

      this.listaEspecies = await this.listasService
        .obterPorId('Espécies')
        .pipe(first())
        .toPromise();

      this.listaTipoDeDado = await this.listasService
        .obterPorId('TipoDeDado')
        .pipe(first())
        .toPromise();

      if (this.modoEdicao) {
        this.examesService
          .obterPorId(this.exameId)
          .pipe(first())
          .subscribe(dados => {
            this.examesLoaderService.carregarExameParaEdicao(dados);
          });
      }
    } catch (error) {
      console.log(error);
    } finally {
      loading.dismiss();
      this.examesCarregado = true;
    }
  }
}
