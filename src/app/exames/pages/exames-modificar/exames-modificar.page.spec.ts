import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ExamesModificarPage } from './exames-modificar.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { ExamesFormCamposComponent } from '../../components/exames-form-campos/exames-form-campos.component';
import { ExamesFormTabelaComponent } from '../../components/exames-form-tabela/exames-form-tabela.component';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { ListasService } from 'src/app/listas/services/listas.service';
import { ExamesService } from '../../services/exames/exames.service';
import { Observable } from 'rxjs';

const routeStub = {
  snapshot: {
    paramMap: {
      get: () => {
        return '123';
      }
    }
  }
};
const examesServiceStub = {
  obterPorId: () => {
    return new Observable(subscriber => {
      subscriber.next({
        nomeAnimal: 'animalNome'
      });
    });
  }
};
const listasServiceStub = {
  obterPorId: () => {
    return new Observable(subscriber => {
      subscriber.next({
        nomeAnimal: 'animalNome'
      });
    });
  }
};
describe('ExamesModificarPage', () => {
  let component: ExamesModificarPage;
  let fixture: ComponentFixture<ExamesModificarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ExamesModificarPage, ExamesFormCamposComponent, ExamesFormTabelaComponent],
      imports: [IonicModule.forRoot(), SharedModule, RouterModule.forRoot([])],
      providers: [
        { provide: ActivatedRoute, useValue: routeStub },
        { provide: ListasService, useValue: listasServiceStub },
        { provide: ExamesService, useValue: examesServiceStub }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(ExamesModificarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('deve ser criado', () => {
    expect(component).toBeTruthy();
  });
});
