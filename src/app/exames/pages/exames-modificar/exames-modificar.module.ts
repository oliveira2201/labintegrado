import { NgModule } from '@angular/core';
import { ExamesModificarPage } from './exames-modificar.page';
import { Routes, RouterModule } from '@angular/router';
import { ExamesFormCamposComponent } from '../../components/exames-form-campos/exames-form-campos.component';
import { ExamesFormTabelaComponent } from '../../components/exames-form-tabela/exames-form-tabela.component';
import { SharedModule } from 'src/app/shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: ExamesModificarPage
  }
];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [
    ExamesModificarPage,
    ExamesFormCamposComponent,
    ExamesFormTabelaComponent,
  ],
})
export class ExamesModificarPageModule { }
