import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'exames',
    children: [
      {
        path: '',
        loadChildren: () =>
          import('../exames/exames-routing.module').then(
            m => m.ExamesRoutingModule
          )
      }
    ]
  },
  {
    path: 'listas',
    children: [
      {
        path: '',
        loadChildren: () =>
          import('../listas/listas-routing.module').then(
            m => m.ListasRoutingModule
          )
      }
    ]
  },
  {
    path: '',
    loadChildren: () =>
      import('./configuracoes-lista/configuracoes-lista.module').then(
        m => m.ConfiguracoesListaPageModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfiguracoesRoutingModule {}
