import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ConfiguracoesListaPage } from './configuracoes-lista.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { RouterModule } from '@angular/router';

describe('ConfiguracoesListaPage', () => {
  let component: ConfiguracoesListaPage;
  let fixture: ComponentFixture<ConfiguracoesListaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConfiguracoesListaPage],
      imports: [IonicModule.forRoot(), SharedModule, RouterModule.forRoot([])]
    }).compileComponents();

    fixture = TestBed.createComponent(ConfiguracoesListaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('deve ser criado', () => {
    expect(component).toBeTruthy();
  });
});
