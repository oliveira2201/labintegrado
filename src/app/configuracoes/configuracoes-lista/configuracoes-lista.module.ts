import { NgModule } from '@angular/core';
import { ConfiguracoesListaPageRoutingModule } from './configuracoes-lista-routing.module';
import { ConfiguracoesListaPage } from './configuracoes-lista.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [SharedModule, ConfiguracoesListaPageRoutingModule],
  declarations: [ConfiguracoesListaPage]
})
export class ConfiguracoesListaPageModule {}
