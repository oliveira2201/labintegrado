import { NgModule } from '@angular/core';
import { ListasListaPage } from './listas-lista.page';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: ListasListaPage
  }
];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [ListasListaPage]
})
export class ListasListaPageModule {}
