import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListasListaPage } from './listas-lista.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { Observable } from 'rxjs';
import { ListasService } from '../../services/listas.service';

const listasServiceStub = {
  obterPorId: () => {
    return new Observable(subscriber => {
      subscriber.next({
        nomeAnimal: 'animalNome'
      });
    });
  }
};

describe('ListasListaPage', () => {
  let component: ListasListaPage;
  let fixture: ComponentFixture<ListasListaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListasListaPage],
      imports: [IonicModule.forRoot(), SharedModule],
      providers: [{ provide: ListasService, useValue: listasServiceStub }]
    }).compileComponents();

    fixture = TestBed.createComponent(ListasListaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('deve ser criado', () => {
    expect(component).toBeTruthy();
  });
});
