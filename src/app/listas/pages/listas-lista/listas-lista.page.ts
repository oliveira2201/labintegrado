import { Component, OnInit } from '@angular/core';
import { Lista } from '../../models/listas.model';
import { ListasService } from '../../services/listas.service';
import { first } from 'rxjs/operators';
import { OverlayService } from 'src/app/shared/services/overlay/overlay.service';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-listas-lista',
  templateUrl: './listas-lista.page.html',
  styleUrls: ['./listas-lista.page.scss']
})
export class ListasListaPage implements OnInit {
  listas: Lista[];
  listaSelecionadaIndice;
  listasCarregadas = false;
  examesServiceSub;

  get listaSelecionada(): Lista | any | object {
    if (this.listas) {
      return this.listas[this.listaSelecionadaIndice];
    }
    return [];
  }
  constructor(private listasService: ListasService, private overlayService: OverlayService) {}

  async ngOnInit() {
    const loading = await this.overlayService.loading({
      message: 'Carregando...'
    });
    try {
      this.examesServiceSub = this.listasService
        .obterTodos()
        .subscribe(listas => (this.listas = listas));
    } catch (error) {
      console.log(error);
    } finally {
      loading.dismiss();
      this.listasCarregadas = true;
    }
  }

  adicionarLista() {
    const lista = {
      id: '',
      nome: '',
      itens: []
    };
    this.overlayService.alert({
      header: 'Selecione a espécie que deseja adicionar',
      inputs: [
        {
          name: 'nome',
          type: 'text'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Ok',
          handler: data => {
            lista.nome = data.nome;
            lista.id = data.nome;
            //@ts-ignore
            this.listasService.criar(lista, data.nome);
            // const totalCampos = this.examesFormService.especies.length;
            // this.selecionarEspecie(totalCampos - 1);
            // this.examesFormService.especieSelecionada.value.nome = data.nome;
          }
        }
      ]
    });
  }

  voltar() {}
  selecionarLista(indice) {
    this.listaSelecionadaIndice = indice;
  }
  async adicionarItem() {
    if (this.listaSelecionada) {
      //@ts-ignore
      this.listaSelecionada.itens.push('');
    }
  }
  async salvar() {
    const loading = await this.overlayService.loading({
      message: 'Salvando...'
    });
    try {
      this.listasService
        .atualizar({
          // @ts-ignore
          id: this.listaSelecionada.id,
          ...this.listaSelecionada
        })
        .then(listaAtualizada => {});
    } catch (error) {
      console.log(error);
    } finally {
      loading.dismiss();
    }
  }
  removerItem(index) {
    // @ts-ignore
    this.listaSelecionada.itens.splice(index, 1);
  }
  trackByFn(index: any, item: any) {
    return index;
  }
  ngOnDestroy() {
    this.examesServiceSub.unsubscribe();
  }
}
