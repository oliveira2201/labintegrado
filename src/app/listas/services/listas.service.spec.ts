import { TestBed } from '@angular/core/testing';

import { ListasService } from './listas.service';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';

const angularFirestoreStub = {
  collection: () => {
    return;
  }
};

describe('ListasService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [{ provide: AngularFirestore, useValue: angularFirestoreStub }]
    })
  );

  it('deve ser criado', () => {
    const service: ListasService = TestBed.get(ListasService);
    expect(service).toBeTruthy();
  });
});
