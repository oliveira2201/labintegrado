import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Repository } from 'src/app/core/classes/repository.class';
import { Lista } from '../models/listas.model';

@Injectable({
  providedIn: 'root'
})
export class ListasService extends Repository<Lista> {
  constructor(db: AngularFirestore) {
    super(db);
    this.init();
  }

  private init(): void {
    this.setCollection('/listas');
  }
}
