import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      // {
      //   path: 'criar',
      //   loadChildren: () =>
      //     import('./pages/exames-modificar/exames-modificar.module').then(
      //       m => m.ExamesModificarPageModule
      //     )
      // },
      // {
      //   path: 'editar/:id',
      //   loadChildren: () =>
      //     import('./pages/exames-modificar/exames-modificar.module').then(
      //       m => m.ExamesModificarPageModule
      //     )
      // },
      {
        path: '',
        loadChildren: () =>
          import('./pages/listas-lista/listas-lista.module').then(
            m => m.ListasListaPageModule
          )
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListasRoutingModule {}
