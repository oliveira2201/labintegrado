import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListasRoutingModule } from './listas-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, ListasRoutingModule]
})
export class ListasModule {}
